﻿/* documentation
 * 001 nanda 13 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;
using System.Text.RegularExpressions;

namespace TransportService.pages
{
    public partial class MobileConfig : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Konfigurasi Mobile";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        //001
        private void BranchDDL()
        {
            if (getBranch() == "")
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
            }
            else
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
            }
        }

        private void TypeValueDDL()
        {

            ddlTypeValue.DataSource = MobileConfigFacade.GetTypeValue();
            ddlTypeValue.DataTextField = "TypeValue";
            ddlTypeValue.DataValueField = "TypeValue";
            ddlTypeValue.DataBind();

        }

        private string SetTextBoxType(string lTypeValue, string lValue)
        {
            if (lTypeValue == "Integer")
            {
                txtValueColor.Visible = false;
                txtValue.Visible = false;
                ddlValue.Visible = false;
                txtValueInt.Visible = true;
                return txtValueInt.Text;
            }

            else if (lTypeValue == "ColorPicker")
            {
                txtValueInt.Visible = false;
                txtValueColor.Visible = true;
                txtValue.Visible = false;
                ddlValue.Visible = false;
                return txtValueColor.Text;
            }

            else if (lTypeValue == "Text")
            {
                txtValueInt.Visible = false;
                ddlValue.Visible = false;
                txtValueColor.Visible = false;
                txtValue.Visible = true;
                return txtValue.Text;
            }
            else
            {
                txtValueInt.Visible = false;
                txtValue.Visible = false;
                txtValueColor.Visible = false;
                ddlValue.Visible = true;
                return ddlValue.SelectedValue;
            }
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public string lParam { get; set; }
        public bool lisScroll { get; set; }

        private void ClearContent()
        {
            txtDesc.Text = "";
            txtID.Text = "";
            txtValue.Text = "";

            txtValue.Visible = true;
            ddlValue.Visible = true;
            txtValueColor.Visible = true;


            btnCancel.Visible = true;
            btnUpdate.Visible = true;
            btnInsert.Visible = true;

            BindData();
            PanelFormMobileConfig.Visible = false;
        }

        private void BindData()
        {
            var listMobileConfig = new List<Core.Model.mdlMobileConfig>();
            listMobileConfig = MobileConfigFacade.GetSearch(ddlSearchBranchID.SelectedValue);

            //nanda buat sync mobileconfig yang kosong 
            if (listMobileConfig.Count == 0)
            {
                MobileConfigFacade.SetDefaultMobileConfig(ddlSearchBranchID.SelectedValue);
                listMobileConfig = MobileConfigFacade.GetSearch(ddlSearchBranchID.SelectedValue);
            }

            var role = getRole();
            foreach (var MobileConfig in listMobileConfig)
            {
                MobileConfig.Role = role;
                btnNew.Visible = role;
            }

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listMobileConfig;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 20;
            pgitems.CurrentPageIndex = PageNumber;
            //lblPage.Text = pgitems.PageCount.ToString();
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }

            RptMobileConfiglist.DataSource = pgitems;
            RptMobileConfiglist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BranchDDL();
                TypeValueDDL();
                ClearContent();

                txtValueColor.Attributes.Add("readonly", "readonly");
            }
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindData();
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData();
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            BindData();
        }

        protected void rptMobileConfig_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkId");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;
                    var lMobileConfig = MobileConfigFacade.GetMobileConfigbyID2(ddlSearchBranchID.SelectedValue, id);

                    PanelFormMobileConfig.Visible = true;
                    txtID.Text = lMobileConfig.ID;
                    txtDesc.Text = lMobileConfig.Desc;

                    SetTextBoxType(lMobileConfig.TypeValue, lMobileConfig.Value);

                }
            }
            if (e.CommandName == "Delete")
            {
                Control control;
                control = e.Item.FindControl("lnkId");
                if (control != null)
                {
                    string id = ((LinkButton)control).Text;
                    string lResult = MobileConfigFacade.DeleteMobileConfigbyID(id);
                }
                ClearContent();
            }
            //PanelFormMobileConfig.Visible = true;
            btnInsert.Visible = false;
            
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string lValue = string.Empty;
            if (txtValue.Visible == true)
            {
                lValue = txtValue.Text;
            }
            else if (txtValueInt.Visible == true)
            {
                lValue = txtValueInt.Text;
            }
            else if (ddlValue.Visible == true)
            {
                lValue = ddlValue.SelectedValue;
            }
            else if (txtValueColor.Visible == true)
            {
                lValue = txtValueColor.Text;
            }

            var lmdlMobileconfiglist = new List<Core.Model.mdlMobileConfig>();
            var lmdlMobileconfig = new Core.Model.mdlMobileConfig();
            lmdlMobileconfig.BranchId = ddlSearchBranchID.SelectedValue;
            lmdlMobileconfig.ID = txtID.Text;
            lmdlMobileconfig.Desc = txtDesc.Text;
            lmdlMobileconfig.Value = lValue;
            lmdlMobileconfiglist.Add(lmdlMobileconfig);

            MobileConfigFacade.UpdateMobileConfig(lmdlMobileconfiglist);


            ClearContent();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            
            var lmdlMobileconfig = new Core.Model.mdlMobileConfig();

            string lValue = SetTextBoxType(ddlTypeValue.SelectedValue, "");

            if (lValue == "")
            {
                Response.Write("<script>alert('Value Cannot Empty');</script>");
                return;
            }

           
            lmdlMobileconfig.ID = txtID.Text;
            lmdlMobileconfig.Desc = txtDesc.Text;
            lmdlMobileconfig.Value = lValue;
            lmdlMobileconfig.TypeValue = ddlTypeValue.SelectedValue;

            string lResult = MobileConfigFacade.InsertMobileConfigbyID(lmdlMobileconfig);

            if (lResult == "0")
            {
                Response.Write("<script>alert('Insert Mobile Config Failed');</script>");
                return;
            }
            else if (lResult.Contains("Already Exist"))
            {
                string lResult2 = string.Format("<script>alert('{0}');</script>", lResult);
                Response.Write(lResult2);
                return;
            }
            else
            {
                Response.Write("<script>alert('Insert Mobile Success');</script>");
            }

            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            PanelFormMobileConfig.Visible = true;
            btnUpdate.Visible = false;
            btnInsert.Visible = true;
            txtID.Enabled = true;

            var lMobileConfig = MobileConfigFacade.GetMobileConfigbyTypeValue(ddlSearchBranchID.SelectedValue, ddlTypeValue.SelectedValue);
            //nanda Buat Validasi jika lMobileConfig
            if (lMobileConfig == null)
            {
                MobileConfigFacade.SetDefaultMobileConfig(ddlSearchBranchID.SelectedValue);
                lMobileConfig = MobileConfigFacade.GetMobileConfigbyTypeValue(ddlSearchBranchID.SelectedValue, ddlTypeValue.SelectedValue);
            }
            SetTextBoxType(lMobileConfig.TypeValue, lMobileConfig.Value);
        }

        protected void btnSearchBranch_Click(object sender, EventArgs e)
        {
        }

        protected void CBIsScroll_CheckedChanged(object sender, EventArgs e)
        {
            //if (CBIsScroll.Checked == true)
            //{
            //    txtValue.Visible = false;
            //    ddlValue.Visible = true;
            //}
            //else
            //{
            //    txtValue.Visible = true;
            //    ddlValue.Visible = false; 
            //}
        }

        protected void btnSearchBranch2_Click(object sender, EventArgs e)
        {
            PageNumber = 0;
            BindData();
            PanelFormMobileConfig.Visible = false;
        }

        protected void ddlTypeValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lMobileConfig = MobileConfigFacade.GetMobileConfigbyTypeValue(ddlSearchBranchID.SelectedValue, ddlTypeValue.SelectedValue);
            SetTextBoxType(lMobileConfig.TypeValue, lMobileConfig.Value);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}