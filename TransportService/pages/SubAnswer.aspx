﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="SubAnswer.aspx.cs" Inherits="TransportService.pages.SubAnswers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
        rel="stylesheet">
    <style>
        th, td
        {
            white-space: nowrap;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Sub Answer</h3>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        <ol class="breadcrumb pull-right">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="Question_Set.aspx">Question Set</a></li>
            <li><a href="Question.aspx">Question</a></li>
            <li><a href="Answer.aspx">Answer</a></li>
        </ol>
    </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="alert alert-warning fade in" style="padding-bottom: 0px; padding-right: 5px;
            padding-top: 10px; padding-left: 15px; margin-bottom: 10px;">
            <h4>
                <i class="fa fa-caret-square-o-right"></i>&nbsp&nbsp Question ID :
                <br />
                <asp:Label ID="lblQuestionID" runat="server" Text="" Style="padding-left: 30px" Font-Bold="True"></asp:Label>
            </h4>
        </div>
    </div>
    <div class="col-md-9 col-sm-12 col-xs-12">
        <div class="alert alert-success fade in" style="padding-bottom: 0px; padding-right: 5px;
            padding-top: 10px; padding-left: 15px; margin-bottom: 10px;">
            <h4>
                <i class="fa fa-caret-square-o-right"></i>&nbsp&nbsp Question Text :
                <br />
                <asp:Label ID="lblQuestionText" runat="server" Style="padding-left: 30px" Text=""
                    Font-Bold="True"></asp:Label>
            </h4>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <asp:DropDownList ID="ddlIsactive" runat="server" CssClass="form-control" 
                            Width="111px" AutoPostBack="True" ontextchanged="ddlIsactive_TextChanged">
                         </asp:DropDownList>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <asp:Button ID="btnNew" runat="server" Text="Add New" class="btn btn-primary pull-right"
                            OnClick="btnNew_Click"></asp:Button>
                    </div>
                    <div class="col-md-12 col-sm-12 col-sm-12 col-xs-12 ">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal" Width="100%">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                            Sequence
                                        </th>
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Answer ID
                                        </th>
                                        <th>
                                            Answer Text
                                        </th>
                                        <th>
                                            Sub Question
                                        </th>
                                        <th>
                                            is Active
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptAnswerlist" runat="server" OnItemCommand="RptAnswerlist_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSubQuestion" Visible="false" runat="server" Text="Sub Question" class="btn btn-success btn-xs"
                                                        CommandName="Sub_Question"></asp:Button>
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-warning btn-xs"
                                                        CommandName="Update"></asp:Button>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" class="btn btn-danger btn-xs"
                                                        CommandName="Delete"></asp:Button>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" Text='<%# Eval("Sequence")%>' ID="dtSequence"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" Text='<%# Eval("No")%>' ID="dtNo"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" Text='<%# Eval("AnswerID")%>' ID="dtAnswerID"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" Text='<%# Eval("AnswerText")%>' ID="dtAnswerText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" Text='<%# Eval("subQuestion")%>' ID="dtsubQuestion"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" Text='<%# Eval("isActive")%>' ID="dtisActive"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="PanelFormAnswer" runat="server" Width="100%">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Form Answer Management
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblNo" runat="server" Text="No" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtNo" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtNo" PlaceHolder="No" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="Label7" runat="server" Text="Sequence" Font-Bold="True" ></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtSeq" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtSeq" PlaceHolder="Sequence" runat="server" class="form-control" type="number"></asp:TextBox>
                                <br />  
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAnswerID" runat="server" Text="Asnwer ID" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtAnswerID" ValidationGroup="valAnswer" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtAnswerID" PlaceHolder="Question ID" runat="server" class="form-control" disabled="disabled"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <br />
                                <asp:Label ID="Label1" runat="server" Text="Answer Text" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtAnswerText" PlaceHolder="Answer Text" runat="server" class="form-control"
                                    TextMode="MultiLine"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtAnswerText" ValidationGroup="valAnswer" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">
                        </div>
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-warning" OnClick="btnUpdate_Click"
                                    ValidationGroup="valAnswer" />
                                <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" OnClick="btnInsert_Click"
                                    ValidationGroup="valAnswer" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <!-- Datatables -->
    <script type="text/javascript" src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script type="text/javascript" src="../vendors/jszip/dist/jszip.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Datatables -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').DataTable({

            });
        });
    </script>
    <!-- /Datatables -->
</asp:Content>
