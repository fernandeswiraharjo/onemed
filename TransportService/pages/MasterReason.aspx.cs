﻿/* Documentation
 * 001 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{
    public partial class MasterReason : System.Web.UI.Page
    {
        public string gRoleId, gUserId;

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Alasan";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        private void ClearContent()
        {
            txtSearch.Text = string.Empty;

            btnCancel.Visible = false;
            btnInsert.Visible = false;
            btnUpdate.Visible = false;

            PanelReason.Visible = false; 

        
        }

        private void BindData(string keyword)
        {
            var listReason = ReasonFacade.GetSearchReason(keyword);

            var role = getRole();
            foreach (var reason in listReason)
            {
                reason.Role = role;
                btnNew.Visible = role;
            }

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listReason;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }

            RptReasonlist.DataSource = pgitems;
            RptReasonlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }

                BindData(txtSearch.Text); 
                ClearContent();
            }
        }

        protected void RptReasonlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                Control control;
                control = e.Item.FindControl("lnkReasonId");
                if (control != null)
                {
                    string lid = ((LinkButton)control).Text; 
                        ReasonFacade.DeleteReason(lid);
                        PanelReason.Visible = false;
                        
                        BindData(txtSearch.Text);

                        return;

                     

                }
            }
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkReasonId");
                if (control != null)
                {

                    txtReasonID.Text = ((LinkButton)control).Text;
                    var lReason = ReasonFacade.GetReasonbyID(txtReasonID.Text);
                    txtReasonName.Text = lReason.Value;
                    txtReasonID.ReadOnly = true;
                    ddlReasonType.SelectedValue = lReason.ReasonType;

                    //btnViewDetail.Visible.Visible = true;
                    btnInsert.Visible = false;
                    btnCancel.Visible = true;
                    btnUpdate.Visible = true;
                    PanelReason.Visible = true;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ReasonFacade.UpdateReason(txtReasonID.Text, txtReasonName.Text, ddlReasonType.SelectedValue);
            Response.Redirect("MasterReason.aspx");
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var lReason = ReasonFacade.GetReasonbyID(txtReasonID.Text);
            if (lReason != null)
            {
                Response.Write("<script>alert('ID already Exist');</script>");
            }
            else
            {
                ReasonFacade.InsertReason(txtReasonID.Text, txtReasonName.Text, ddlReasonType.SelectedValue);

                Response.Write("<script>alert('Insert Success');</script>");
            }
            Response.Redirect("MasterReason.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            PanelReason.Visible = false;
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            txtReasonID.Text = string.Empty;
            txtReasonName.Text = string.Empty;
            btnInsert.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = true;
            PanelReason.Visible = true;
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;

            BindData(txtSearch.Text); 
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData(txtSearch.Text); 
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            BindData(txtSearch.Text); 
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PageNumber = 0;
            BindData(txtSearch.Text); 
        }
    }
}