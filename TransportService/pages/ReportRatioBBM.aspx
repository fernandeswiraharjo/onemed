﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportRatioBBM.aspx.cs" Inherits="TransportService.pages.ReportRatioBBM" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="page-title">
        <div class="title_left">
            <h3>
                Form Report Ratio BBM</h3>
        </div>
        <div class="title_right">
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
           
            <div class="x_content">
                <br />
                                <div class="form-group">
                                <fieldset>
                                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                                    </asp:ScriptManager>
                                    <p>
                                        <asp:Label ID="lblBranchD" runat="server" Text="Branch ID" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                

                                        <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" 
                                            Style="margin-left:0px; width:150px" AutoPostBack="True"
                                            onselectedindexchanged="ddlSearchBranchID_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </p>

                                    <%--<p>

                                        <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:TextBox ID="txtEmployeeID" runat="server" 
                                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Search Employee"></asp:TextBox>--%> <%--001 nanda--%>
                                       

                                        <%--<asp:Button ID="btnShowEmployee" runat="server" CssClass="btn btn-success" Text="Show Employee" onclick="btnShowEmployee_Click" style="margin-left:10px"/>
                                        <asp:Label ID="lblValblEmployee" runat="server" style="color:Red;" visible="false"></asp:Label>--%> <%-- 002 fernandes --%>
                                        
                                        <%--<asp:Panel ID="Panel1" CssClass="form-control" Height="200px" Width="400px" runat="server" ScrollBars="Vertical" style="margin-left:10px">
                                        <asp:RadioButtonList ID="rbEmployee" runat="server">
                                        </asp:RadioButtonList>
                                        </asp:Panel>
                                        
                                        </p>--%>

                                        <p>

                                        <asp:Label ID="lblVehicleID" runat="server" Text="Vehicle" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:Label ID="LabelVehicle" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        
                                        <asp:DropDownList ID="ddlSearchVehicleID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                                        </asp:DropDownList>
                                        
                                        </p>
                                                
                                    <p>
                                        <asp:Label ID="lbl2" runat="server" Text="Date" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:TextBox ID="txtdate1" runat="server" 
                                            Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Start Date"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="- Start Date Required -"
                                                ControlToValidate="txtdate1" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>

                                                <script type="text/javascript">
                                                    var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate1.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                                        </script>
                                                
                                        
                                        
                                        <asp:TextBox ID="txtdate2" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="End Date" style="margin-left:10px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="- End Date Required -"
                                                ControlToValidate="txtdate2" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>

                                                <script type="text/javascript">
                                                    var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate2.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                                        </script>
                                        
                                    </p>
                                    <br />
                                    <asp:Button ID="btnShow" runat="server" Text="Show Report PDF" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                                        <asp:Button ID="btnShowExcel" runat="server" Text="Show Report Excel" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShowExcel_Click" ValidationGroup="valShowReport" />
                                        </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
            <%-----------------------------------------------------------// Start Cut Form //--------------------------------------------------------------------------------------%>
            <br />
            <br />
            <div class="col-md-12 col-xs-12">
                <asp:Panel ID="PanelReportRatioBBM" runat="server" Height="500px" Width="100%" CssClass="datagrid">
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                        Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                        WaitMessageFont-Size="14pt">
                        <LocalReport ReportPath="Report\ReportRatioBBM.rdlc">
                        </LocalReport>
                    </rsweb:ReportViewer>
                </asp:Panel>
            </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
