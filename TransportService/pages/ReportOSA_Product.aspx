﻿<%@ Page Title="Report OSA Product By Customer" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="ReportOSA_Product.aspx.cs" Inherits="TransportService.pages.ReportOSA_Product" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                OSA Stock Product
            </h3>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                <div class="form-group">
                <asp:ScriptManager ID="ScriptManager2" runat="server">
                </asp:ScriptManager>
                <div class="col-md-12">
                        <input action="action" type="button" class="btn btn-sm btn-embossed btn-primary" value="Back"  onclick="window.history.go(-1);" />
                    </div>
                    <%--<div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:TextBox ID="txtCustomerID" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12"
                            PlaceHolder="Search Customer"></asp:TextBox>
                        <asp:Button ID="btnShowCustomer" runat="server" CssClass="btn btn-success" Text="Show Customer"
                            Style="margin-left: 10px" OnClick="btnShowCustomer_Click" />
                        <asp:Label ID="lblValblProduct" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                        <asp:Panel ID="PnlCustomer" CssClass="form-control" Height="200px" Width="400px" runat="server"
                            ScrollBars="Vertical" Style="margin-left: 10px">
                            <asp:RadioButtonList ID="rdCustomer" runat="server">
                            </asp:RadioButtonList>
                        </asp:Panel>
                        <br />
                        <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="SELECT ALL"
                            OnClick="btnSelectAll_Click" Style="margin-left: 10px" />
                        <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL"
                            OnClick="btnUnSelectAll_Click" Style="margin-left: 10px" />
                    </div>--%>
                    <%--<div class="col-md-12 col-xs-12">
                        <asp:Button ID="btnShow" runat="server" Text="Show Report" class="btn btn-sm btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                    </div>--%>
                    <div class="col-md-12">
                            <asp:Button ID="btnExportExcel" runat="server" Text="Export To Excel" class="btn btn-embossed btn-primary pull-right"
                            Font-Size="14px" OnClick="btnExportExcel_Click" 
                                ValidationGroup="valShowReport" />
                        </div>
                    <br />
                    <div class="datagrid" style="width: 1000px">
                        <table style="width: 100%; margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th>
                                        Product
                                    </th>
                                    <asp:Repeater ID="RptWeeks" runat="server">
                                        <ItemTemplate>
                                            <th>
                                                <asp:Label ID="lWeek" runat="server" Text='<%# Eval("Week") %>'></asp:Label>
                                            </th>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <asp:Repeater ID="RptOSAProductlist" runat="server" OnItemDataBound="ItemBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Product" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label> - <asp:Label ID="ProductName" runat="server" Text='<%# Eval("ProductName") %>'></asp:Label>
                                                </td>
                                                <%--Repeater for child data percentage--%>
                                                <asp:Repeater ID="RptChildStocklist" runat="server">
                                                    <ItemTemplate>
                                                        <td>
                                                            <%# Eval("Stock") %>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </tbody>
                            <%--<tfoot>
                                    <tr>
                                        <td colspan="8">
                                            <div id="paging">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                    runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <li>
                                                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                </ul>
                                            </div>
                                         </td>
                                    </tr>
                                </tfoot>--%>
                        </table>
                    </div>
                    <div class= "col-md-12">
                    <asp:Panel ID="PanelReport" runat="server" Height="500px" Width="100%" 
                            CssClass="datagrid" Visible="false" > 
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                            Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                            WaitMessageFont-Size="14pt">
                            <LocalReport ReportPath="Reports\ReportOSAStock.rdlc">
                            </LocalReport>
                        </rsweb:ReportViewer>
                    </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
