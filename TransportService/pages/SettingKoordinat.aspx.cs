﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class SettingKoordinat : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StartDate.Text = DateTime.Now.Date.ToString("dd MMMM yyyy");
                EndDate.Text = DateTime.Now.Date.ToString("dd MMMM yyyy");
                BindData();
            }
        }

        private void BindData()
        {

            StartDate.Text = DateTime.Today.Date.ToString("dd MMMM yyyy");

            ////if (Session["Access"] == "ADMIN")
            ////{
            ////ddlCabang.DataSource = Core.Manager.Branch.GetBranchCostum();
            if (getBranch() == "")
            {
                ddlCabang.DataSource = Core.Manager.BranchFacade.LoadBranch2(getBranch());
            }
            else
            {
                ddlCabang.DataSource = Core.Manager.BranchFacade.LoadSomeBranch(getBranch());
            }
            ////}

            ////else
            ////{
            ////    if (Session["Branch"] == null || Session["Branch"] == "")
            ////        Response.Redirect("Login.aspx");
            //    //var branch = Core.Manager.Branch.GetBranchDetailCostum(Session["Branch"].ToString());
            //    //ddlCabang.DataSource = branch;
            //}

            ////ddlCabang.DataValueField = "szId";
            ////ddlCabang.DataTextField = "szName";
            ////ddlCabang.DataBind();

            ddlCabang.DataValueField = "BranchID";
            ddlCabang.DataTextField = "BranchName";
            ddlCabang.DataBind();

            string keyword = txtSearch.Text;
            //gvCustomer.DataSource = Customer.GetCustomer().Where(fld => fld.szBranchId.Equals(ddlCabang.SelectedValue)).Where(fld => fld.szId.ToLower().Contains(keyword.ToLower()) || fld.szName.ToLower().Contains(keyword.ToLower()) || fld.szAddress.ToLower().Contains(keyword.ToLower())).ToList();
            gvCustomer.DataSource = CustomerFacade.GetSearch2(ddlCabang.SelectedValue,keyword);
            gvCustomer.DataBind();
        }

        protected void ddlCabang_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string keyword = txtSearch.Text;
            //gvCustomer.DataSource = Customer.GetCustomer().Where(fld => fld.szBranchId.Equals(ddlCabang.SelectedValue)).Where(fld => fld.szId.ToLower().Contains(keyword.ToLower()) || fld.szName.ToLower().Contains(keyword.ToLower()) || fld.szAddress.ToLower().Contains(keyword.ToLower())).ToList();
            //gvCustomer.DataBind();

            string keyword = txtSearch.Text;
            gvCustomer.DataSource = CustomerFacade.GetSearch2(ddlCabang.SelectedValue,keyword);
            gvCustomer.DataBind();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //var listCustomer = new List<Core.Model.SFA_Customer>();
            //var customer = Customer.GetCustomerDetail(txtCustomerId.Text);
            //if (customer == null)
            //{

            //}
            //else
            //{
            //    listCustomer.Add(customer);
            //    rptMarkers.DataSource = listCustomer;
            //    rptMarkers.DataBind();
            //    dgKunjungan.DataSource = DocCall.GetKoordinatKunjungan(cldFrom.SelectedDate, cldTo.SelectedDate, txtCustomerId.Text);
            //    dgKunjungan.DataBind();

            //    //var listVisit = DocCall.GetKoordinatKunjungan(cldFrom.SelectedDate, cldTo.SelectedDate, txtCustomerId.Text);
            //    //var visit = listVisit[1];
            //    //visit.szDate =visit.szDate;
            //    //visit.szLongitude =visit.szLongitude;
            //    //visit.szLangitude = visit.szLangitude;

            //    //var listVisitNew = new List<Core.Model.spGetKoordinatKunjunganResult>();
            //    //listVisitNew.Add(visit);
            //    //rptVisit.DataSource = listVisitNew;
            //    //rptVisit.DataBind();

                
            //}
            ////Session["txtCustomerId"] = txtCustomerId.Text;
            ////Session["CldFrom"] = cldFrom.SelectedDate;
            ////Session["CldTo"] = cldTo.SelectedDate;
            ////string queryString = "SettingKoordinatPop.aspx";
            ////string newWin = "window.open('" + queryString + "');";
            ////ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);

            Session["txtCustomerId"] = txtCustomerId.Text;
            Session["CldFrom"] = StartDate.Text;
            Session["CldTo"] = EndDate.Text;
            string queryString = "SettingKoordinatPop.aspx";
            string newWin = "window.open('" + queryString + "');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void gvCustomer_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {

        }

        protected void gvCustomer_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            ////if (e.CommandName == "id")
            ////{
            ////    txtCustomerId.Text = e.Item.Cells[1].Text;
            ////}

            if (e.CommandName == "id")
            {
                txtCustomerId.Text = e.Item.Cells[1].Text;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ////string keyword = txtSearch.Text;
            //gvCustomer.DataSource = Customer.GetCustomer().Where(fld => fld.szBranchId.Equals(ddlCabang.SelectedValue)).Where(fld => fld.szId.ToLower().Contains(keyword.ToLower()) || fld.szName.ToLower().Contains(keyword.ToLower()) || fld.szAddress.ToLower().Contains(keyword.ToLower())).ToList();
            ////gvCustomer.DataBind();

            string keyword = txtSearch.Text;
            gvCustomer.DataSource = CustomerFacade.GetSearch2(ddlCabang.SelectedValue,keyword);
            gvCustomer.DataBind();
        }

        //protected void dgKunjungan_ItemCommand(object source, DataGridCommandEventArgs e)
        //{
        //    if (e.CommandName == "id")
        //    {
        //        var listVisit = new List<Core.Model.spGetKoordinatKunjunganResult>();
        //        var visit = new Core.Model.spGetKoordinatKunjunganResult();
        //        visit.szDate = e.Item.Cells[1].Text;
        //        visit.szLongitude = e.Item.Cells[2].Text;
        //        visit.szLangitude = e.Item.Cells[3].Text;
        //        listVisit.Add(visit);
        //        rptVisit.DataSource = listVisit;
        //        rptVisit.DataBind();
        //        TextBox1.Text = e.Item.Cells[2].Text;
        //        TextBox2.Text = e.Item.Cells[3].Text;
        //    }
        //}
    }
}