﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class ReportOOS_SKUAccountBranch : System.Web.UI.Page
    {
        private List<Core.Model.mdlOOSByAccountBranchSKU> listAccountProductOOS;
        private List<int> listWeek;
        private int year;
        private string branchID;
        private string productID;
        private string role;
        protected void Page_Load(object sender, EventArgs e)
        {
            int fromWeek = Convert.ToInt32(Request.QueryString["fromweek"]);
            int toWeek = Convert.ToInt32(Request.QueryString["toweek"]);
            productID = Request.QueryString["productid"];
            branchID = Request.QueryString["branchid"];
            role = Request.QueryString["role"];
            year = Convert.ToInt32(Request.QueryString["year"]);

            listAccountProductOOS = OOSFacade.GetOOSByAccountBranchSKU(Convert.ToInt16(fromWeek), Convert.ToInt16(toWeek), branchID, productID, Convert.ToInt16(year),role);
            listWeek = listAccountProductOOS.Select(fld => fld.VisitWeek).Distinct().OrderBy(fld=>fld).ToList();

            var listParentAccount = listAccountProductOOS.Select(fld => new { fld.Account, fld.ProductID, fld.ProductName }).Distinct().ToList();

            

            rptHeaderReportAccount.DataSource = listWeek;
            rptHeaderReportAccount.DataBind();
            Label lblControl = rptHeaderReportAccount.Controls[0].Controls[0].FindControl("lblHeader") as Label;
            lblControl.Text = "ACCOUNT";

            

            rptParentReportAccount.DataSource = listParentAccount;
            rptParentReportAccount.DataBind();

            var listChartAccount = new List<Core.Model.mdlOOSChart>();
            int chartID = 1;
            foreach (var acc in listParentAccount)
            {
                var tempListOOS = listAccountProductOOS.Where(fld => fld.Account.Equals(acc.Account));
                //var listChartBranchData = new List<Core.Model.mdlOOSChartData>();
                var listChartBranchData = tempListOOS.Select(fld => new { fld.VisitWeek, fld.OOS }).OrderBy(fld => fld.VisitWeek).ToList();

                string json = Core.Services.RestPublisher.Serialize(listChartBranchData);
                var chart = new Core.Model.mdlOOSChart();
                chart.ID = "chart" + chartID.ToString();
                chart.Area = acc.Account;
                chart.json = json;
                listChartAccount.Add(chart);

                chartID++;

            }


            rptBranchChart.DataSource = listChartAccount;
            rptBranchChart.DataBind();

            var listOOSGrandTotal = new List<decimal>();
            foreach (int i in listWeek)
            {
                decimal totalOOS = listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.OutOfStock);
                decimal totalListed = totalOOS + listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.Listed);
                decimal average = 0;
                if (totalListed != 0)
                {
                    average = (totalOOS / totalListed) * 100;
                }
                
                

                listOOSGrandTotal.Add(decimal.Round(average, 2));
            }

            rptGrandTotal.DataSource = listOOSGrandTotal;
            rptGrandTotal.DataBind();


        }

        protected void ReportAccountItemBound(object sender, RepeaterItemEventArgs args)
        {
            RepeaterItem ri = args.Item;
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblAccount = ri.FindControl("lblAccount") as Label;
                Label lblProductID = ri.FindControl("lblProductID") as Label;
                string account = lblAccount.Text;
                Repeater childRepeater = (Repeater)args.Item.FindControl("rptChildAccountStock");

                var listChild = listAccountProductOOS.Where(fld => fld.Account.Equals(account) & fld.ProductID.Equals(lblProductID.Text)).ToList();
                var listChildFinal = new List<Core.Model.mdlOOSByAccountBranchSKU>();
                foreach (int i in listWeek)
                {
                     var tempList = listChild.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                     if (tempList != null)
                     {
                         var model = new Core.Model.mdlOOSByAccountBranchSKU();
                         model.Account = tempList.Account;
                         model.OOS = tempList.OOS;
                         model.ProductID = tempList.ProductID;
                         model.ProductName = tempList.ProductName;
                         model.VisitWeek = tempList.VisitWeek;
                         model.Link = "ReportOOS_SKUCustomer.aspx?week=" + model.VisitWeek + "&account=" + model.Account + "&productid=" + model.ProductID + "&year=" + year + "&branch=" + branchID + "&role=" + role;
                         listChildFinal.Add(model);
                     }
                     else
                     {
                         var model = new Core.Model.mdlOOSByAccountBranchSKU();

                         //model.Account = "";
                         model.VisitWeek = i;
                         //model.Link = "";
                         //model.OOSCustomer = 0;
                         //model.ProductID = "";
                         //model.ProductName = "";
                         listChildFinal.Add(model);
                     }
                }


                listChildFinal = listChildFinal.OrderBy(fld => fld.VisitWeek).ToList();


                childRepeater.DataSource = listChildFinal;
                childRepeater.DataBind();
            }
        }
    }


}