﻿<%@ Page Title="Update Call Plan Detail" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="UpdateCallPlanDetail.aspx.cs" Inherits="UpdateCallPlanDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                MOVE CALL PLAN</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:Label ID="lblHeading" runat="server" Text=" List Call Plan "></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="lblSearch" runat="server" Text=" Employee ID : " Width="110px" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Font-Bold="True" style="margin-left:-10px; margin-top:5px;"></asp:Label>
                                <asp:TextBox ID="txtSearch" runat="server" Style="margin-left: 0px; margin-top: 0px"
                                    Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Employee ID"></asp:TextBox>
                                <%--<asp:Label ID="lblDate" runat="server" Text=" Date : "></asp:Label>--%>
                                <%--<asp:TextBox ID="txtDate" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                            Width="150px" CssClass="tb5" placeholder="*Required..."></asp:TextBox>--%>
                                <%--<asp:RequiredFieldValidator ID="Required1" runat="server" ErrorMessage="*" ControlToValidate="txtDate" ValidationGroup="callplan" ForeColor="#FF3300"
                                                SetFocusOnError="True">
                                        </asp:RequiredFieldValidator>--%>
                                <%--<script type="text/javascript">
                                            var picker1 = new Pikaday(
                                {
                                    field: document.getElementById('<%=txtDate.ClientID%>'),
                                    firstday: 1,
                                    minDate: new Date('2000-01-01'),
                                    maxDate: new Date('2020-12-31'),
                                    yearRange: [2000, 2020]
                                    //                                    setDefaultDate: true,
                                    //                                    defaultDate : new Date()
                                }
                                );
                                </script>--%>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search" OnClick="btnSearch2_Click"
                                    CssClass="btn btn-primary" ValidationGroup="callplan" style="margin-left:10px"></asp:Button>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancel_Click" />
                            </div>
                            <br />
                            <br />
                            <div class="datagrid" style="width: 1000px">
                                <table style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                Call Plan ID
                                            </th>
                                            <th>
                                                Employee ID
                                            </th>
                                            <th>
                                                Date
                                            </th>
                                            <th>
                                                Car Number
                                            </th>
                                            <th>
                                                Branch ID
                                            </th>
                                            <%--<th>
                                                    </th>--%>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="RptCallPlanlist" runat="server" OnItemCommand="rptCallPlan_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkCallPlanId" runat="server" Text='<%# Eval("CallPlanID") %>'
                                                            OnClientClick="return confirm('Are you sure you want to Move into this Call Plan ?');"
                                                            CommandName="Link"></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <%# Eval("EmployeeID") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("Date") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("VehicleID") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("BranchID") %>
                                                    </td>
                                                    <%--<td>
                                                                <asp:LinkButton ID="lnkDelete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this Product?');"
                                                                    runat="server" CommandName="Delete" />
                                                            </td>--%>
                                                </tr>
                                                </tbody>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5">
                                                    <div id="paging">
                                                        <ul>
                                                            <li>
                                                                <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                            <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                            runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <li>
                                                                <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                        </ul>
                                                    </div>
                                            </tr>
                                        </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</asp:Content>
