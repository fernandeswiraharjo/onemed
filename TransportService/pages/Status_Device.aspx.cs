﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class StatusDevices : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId;
        public Core.Model.User vUser;

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }


        public string lParam { get; set; }

        private void ClearContent()
        {
            BindData();
        }

        private void BindData()
        {
            var mdlStatusDeviceList = new List<Core.Model.mdlStatusDevice>();
            DateTime lParamDate = DateTime.Now;
            if (txtdate.Text != ""){
                lParamDate = DateTime.Parse(txtdate.Text);
            }
            mdlStatusDeviceList = CallPlanFacade.GetStatusDevice(lParamDate, cbDownload.Checked);

            RptStatusDevicelist.DataSource = mdlStatusDeviceList;
            RptStatusDevicelist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearContent();
            }
        }

        protected void RptStatusDevicelist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtDevice = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtDevice")).Text);
            var dtEmployeeName = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtEmployeeName")).Text);
            var dtIsDownload = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtIsDownload")).Text);
            var dtIsFinish = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtIsFinish")).Text);

            if (e.CommandName == "ShowStatus")
            {

            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            BindData();
        }
    }
}