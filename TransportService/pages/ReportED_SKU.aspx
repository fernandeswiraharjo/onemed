﻿<%@ Page Title="Report ED Product" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="ReportED_SKU.aspx.cs" Inherits="TransportService.pages.ReportED_SKU" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<%--<!-- gauge.js -->
    <script>
        var opts = {
            lines: 12,
            angle: 0,
            lineWidth: 0.4,
            pointer: {
                length: 0.75,
                strokeWidth: 0.042,
                color: '#1D212A'
            },
            limitMax: 'false',
            colorStart: '#1ABC9C',
            colorStop: '#1ABC9C',
            strokeColor: '#F0F3F3',
            generateGradient: true
        };
        var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

        gauge.maxValue = 6000;
        gauge.animationSpeed = 32;
        gauge.set(3200);
        gauge.setTextField(document.getElementById("gauge-text"));
    </script>
    <!-- /gauge.js -->--%>

    <!-- morris.js -->
    <script type="text/javascript" src="js/journey/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../vendors/raphael/raphael.min.js"></script>
    <script type="text/javascript"src="../vendors/morris.js/morris.min.js"></script>
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                ED Product
            </h3>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <%--<<box parameter--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                    <input action="action" type="button" class="btn btn-sm btn-embossed btn-primary" value="Back"  onclick="window.history.go(-1);" />
                    <div class="clearfix">
                    </div>
                
            </div>
            <div class="x_content">
                <div class="form-group">
                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                    </asp:ScriptManager>

                    <div class="col-md-2 col-xs-2"> 
                        NAME OF STORE
                    </div>
                    <div class="col-md-10 col-xs-10"> 
                        : <asp:Label ID="lblCustomer" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="col-md-2 col-xs-2"> 
                        ACCOUNT
                    </div>
                    <div class="col-md-10 col-xs-10"> 
                        : <asp:Label ID="lblAccount" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="col-md-2 col-xs-2"> 
                        CHANNEL
                    </div>
                    <div class="col-md-10 col-xs-10"> 
                        : <asp:Label ID="lblChannel" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="col-md-2 col-xs-2"> 
                        GrandTotal 
                    </div>
                    <div class="col-md-10 col-xs-10"> 
                        : <b><asp:Label ID="lblGrandTotal" runat="server" Text="Label"></asp:Label></b>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <%--box parameter>>--%>
    <%--<<tab chart and table--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    <i class="fa fa-bars"></i>Charts & Tables<small>Area</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    
                </ul>
                <div class="clearfix">
                </div>
                
            </div>
            <div class="x_content">
            <div class="datagrid" style="width: 100%">
                                <table style="width: 100%; margin-bottom: 0px;">
                                    <thead>
                                        <tr>
                                            <th>
                                                SKU
                                            </th>
                                            <asp:Repeater ID="RptSisaStockType" runat="server">
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label ID="lSisaStockTypeID" runat="server" Text='<%# Eval("SisaStockTypeID")%>'></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <asp:Repeater ID="RptProductlist" runat="server" OnItemDataBound="ItemBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%# Eval("ProductID") %> - <%# Eval("ProductName")%>
                                                        </td>
                                                        <%--Repeater for child data percentage--%>
                                                        <asp:Repeater ID="RptChildEDlist" runat="server">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <%# Eval("Value")%>
                                                                    
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <b>Total</b>
                                            </td>
                                            <asp:Repeater ID="RptChildGrandTotal" runat="server">
                                                <ItemTemplate>
                                                    <td>
                                                        <b>
                                                            <%# Eval("Value")%> 
                                                        </b>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </tbody>
                                    <%--<tfoot>
                                    <tr>
                                        <td colspan="8">
                                            <div id="paging">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                    runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <li>
                                                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                </ul>
                                            </div>
                                         </td>
                                    </tr>
                                </tfoot>--%>
                                </table>
                            </div>
            </div>
            
        </div>
        <asp:Panel ID="PanelReport" runat="server" Height="500px" Width="100%" CssClass="datagrid"
                            Visible="False">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                WaitMessageFont-Size="14pt">
                                <LocalReport ReportPath="Reports\ReportOSAAccount.rdlc">
                                </LocalReport>
                            </rsweb:ReportViewer>
                        </asp:Panel>
    </div>
    <%--tab chart and table>>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">

</asp:Content>
