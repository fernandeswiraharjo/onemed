﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core;
using Core.Manager;

namespace TransportService.pages
{
    public partial class Index : System.Web.UI.Page
    {
        //public string jsonOSA = "";
        //public string jsonOOS = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ////lblUser.Text = Session["User"].ToString();
            //var weekFrom = OSAFacade.GetIso8601WeekOfYear(DateTime.Now) - 1;
            //var weekTo = OSAFacade.GetIso8601WeekOfYear(DateTime.Now);
            ////var weekFrom = 42;
            ////var weekTo =43;
            //var osaArea = OSAFacade.LoadOSADashboard(weekFrom.ToString(), weekTo.ToString(), DateTime.Now.Year.ToString());
            //jsonOSA = Core.Services.RestPublisher.Serialize(osaArea);
            //var oosArea = OOSFacade.LoadOOSDashboard(weekFrom.ToString(), weekTo.ToString(), DateTime.Now.Year.ToString());
            //jsonOOS = Core.Services.RestPublisher.Serialize(oosArea);

            var listLiveTracking = new List<Core.Model.mdlLiveTracking>();
            listLiveTracking.AddRange(Core.Manager.TrackingFacade.GetLiveTrackingAll());
            rptLiveTrackingMarkers.DataSource = listLiveTracking;
            rptLiveTrackingMarkers.DataBind();
        }
    }
}