﻿/* documentation
 * 001 nanda 06/11/2017 - Optimasi Looping Data
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Globalization;
using System.Data;
using System.IO;
using Core.Model;
using Microsoft.Reporting.WebForms;
using System.Web.Services;
using System.Web.Script.Serialization;

namespace TransportService.pages
{

    public partial class ReportED_SKU : System.Web.UI.Page
    {
        private int gNO = 0;
        private string gTypeID = string.Empty;
        private string gWeek = string.Empty;
        private string gAccount = string.Empty;
        private string gCustomer = string.Empty;
        private string gYear = string.Empty;
        private string gArea = string.Empty;
        private string gRole = string.Empty;
        private List<Core.Model.mdlSisaStockType> gSisaStockTypeIDList = new List<Core.Model.mdlSisaStockType>();
        private List<Core.Model.mdlGraphData> gTemp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
        private List<Core.Model.mdlRptEDbySKU> gMdlEDbySKUList = new List<Core.Model.mdlRptEDbySKU>();
        private IEnumerable<Core.Model.mdlRptEDbySKU> gMdlEDbySKUProduct;

        private void ClearContent()
        {
            gTemp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
        }

        //<<chartcode
        private void GetChartData(List<Core.Model.mdlOSAData> mdlOSAArealist)
        {
            String lData = string.Empty;
            String lPercent = string.Empty;
            //List<Core.Model.mdlGraphData> Temp_dataList = new List<Core.Model.mdlGraphData>();
            Core.Model.mdlGraphData details = new Core.Model.mdlGraphData();
            foreach (var lParam in mdlOSAArealist)
            {
                details = new Core.Model.mdlGraphData();
                details.element = lParam.ID;

                if (lParam.Percent != "")
                {
                    lData += "{ weeks: '" + lParam.Week + "', percents: " + lParam.Percent.Replace("%", "") + "},";
                }

                details.data = lData;
                lPercent = lParam.Percent;
            }

                if(details.data != null)
                    gTemp_dataList.Add(details);
        }
        //chartcode>>

        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            //Dapatkan CustomerName & Account
            int i = gNO;
            string lProductID = gMdlEDbySKUProduct.ToArray()[i].ProductID;
            //string lSisaStockType = gMdlEDbySKUProduct.ToArray()[i].SisaStockTypeID;
            gNO += 1;
            if (lProductID == "" )
            {
                return;
            }

            var EDChildList = gMdlEDbySKUList.Where(fld => fld.ProductID.Equals(lProductID)).ToList();

            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildEDlist");
                childRepeater.DataSource = EDChildList;
                childRepeater.DataBind();
            }

            
            //GetChartData(listOSAArea); //chartcode
        }

        private void BindData()
        {

            if (gCustomer == null)
            {
                var mdlSisaStockType = new Core.Model.mdlSisaStockType();
                mdlSisaStockType.SisaStockTypeID = gTypeID;
                gSisaStockTypeIDList.Add(mdlSisaStockType);
                gMdlEDbySKUList = EDFacade.LoadED_SKU(gSisaStockTypeIDList, gArea, gAccount, gWeek, gYear, "",gRole);
            }
            else
            {
                if (gTypeID == "summary")
                {
                    gSisaStockTypeIDList = EDFacade.GetlistSisaStockTypeID();
                    gSisaStockTypeIDList.Remove(gSisaStockTypeIDList.Last());
                }
                else
                {
                    var mdlSisaStockType = new Core.Model.mdlSisaStockType();
                    mdlSisaStockType.SisaStockTypeID = gTypeID;
                    gSisaStockTypeIDList.Add(mdlSisaStockType);
                }
                
                gMdlEDbySKUList = EDFacade.LoadED_SKU(gSisaStockTypeIDList, gArea, gAccount, gWeek, gYear, gCustomer,gRole);
            }

            

            //<<Product
            gMdlEDbySKUProduct = gMdlEDbySKUList.GroupBy(grp => new { grp.ProductID, grp.ProductName }).Select(fld => new mdlRptEDbySKU
                                                                    {
                                                                        ProductID = fld.Key.ProductID,
                                                                        ProductName = fld.Key.ProductName
                                                                    }); 
            PagedDataSource pgitems2 = new PagedDataSource();
            pgitems2.DataSource = gMdlEDbySKUProduct.ToArray();
            RptProductlist.DataSource = pgitems2;
            RptProductlist.DataBind();
            //Product>>

            //<<GrandTotal
            var gMdlEDbySKUTotal = gMdlEDbySKUList
                                                .GroupBy(grp => new { grp.VisitWeek, grp.SisaStockTypeID })
                                                .Select(fld => new mdlRptEDbySKU
                                                {
                                                    VisitWeek = fld.Key.VisitWeek,
                                                    SisaStockTypeID = fld.Key.SisaStockTypeID,
                                                    Value = fld.Sum(x => x.Value)
                                                });

            decimal GrandTotal = gMdlEDbySKUTotal.Sum(fld => fld.Value);
            lblGrandTotal.Text = GrandTotal.ToString();

            PagedDataSource pgitemsGT = new PagedDataSource();
            pgitemsGT.DataSource = gMdlEDbySKUTotal.ToArray();
            RptChildGrandTotal.DataSource = pgitemsGT;
            RptChildGrandTotal.DataBind();
            //GrandTotal>>

            //<<SisaStock
            PagedDataSource pgitemSisaType = new PagedDataSource();
            pgitemSisaType.DataSource = gSisaStockTypeIDList;
            RptSisaStockType.DataSource = pgitemSisaType;
            RptSisaStockType.DataBind();

            

            //<<chartcode
            //PagedDataSource pgitems3 = new PagedDataSource();
            //pgitems3.DataSource = Globals.Temp_dataList;
            //RptChartlist.DataSource = pgitems3;
            //RptChartlist.DataBind();
            //chartcode>>
            
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gNO = 0;
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                ClearContent();

                gTypeID = Request.QueryString["TypeID"];
                gWeek = Request.QueryString["Week"];
                gAccount = Request.QueryString["Account"];
                gCustomer = Request.QueryString["Customer"];
                gYear = Request.QueryString["Year"];
                gArea = Request.QueryString["Area"];
                gRole = Request.QueryString["role"];
                string lChannel = Request.QueryString["Channel"];

                if (gCustomer == null)
                {
                    lblCustomer.Text = "ALL";
                    lblChannel.Text = "ALL";
                }
                else
                {
                    lblCustomer.Text = gCustomer;
                    lblChannel.Text = lChannel;
                }
                

                lblAccount.Text = gAccount;
                BindData();
            }
        }

        protected void RptOSAArealist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ////Check Checkbox Branch
            //if (CheckArea() == true)
            //    return;

            //List<String> listBranch = new List<String>();
            //foreach (ListItem item in blBranch.Items)
            //{
            //    if (item.Selected)
            //    {// If the item is selected, add the value to the list.

            //        listBranch.Add(item.Value);
            //    }
            //}

            
            //var mdlOSAAreaExcel = Core.Manager.OSAFacade.LoadOSAAreaExcel(listBranch, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue);
            //ReportDataSource rds = new ReportDataSource("DataSet1", mdlOSAAreaExcel);
            //ReportViewer1.LocalReport.DataSources.Clear();
            //ReportViewer1.LocalReport.DataSources.Add(rds);

            //ReportViewer1.ShowReportBody = true;
            //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportOSAArea.rdlc");
            //ReportViewer1.LocalReport.Refresh();

            ////download excel file
            //ReportViewer1.ProcessingMode = ProcessingMode.Local;

            //Warning[] warnings;
            //string[] streamIds;
            //string mimeType = string.Empty;
            //string encoding = string.Empty;
            //string extension = string.Empty;
            //byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            //Response.Buffer = true;
            //Response.Clear();
            //Response.ContentType = mimeType;
            //Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportOSA_Area" + "." + extension);
            //Response.BinaryWrite(bytes); // create the file
            //Response.Flush(); // send it to the client to download
        }

    }
}