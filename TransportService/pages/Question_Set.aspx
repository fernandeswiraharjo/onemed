﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="Question_Set.aspx.cs" Inherits="TransportService.pages.QuestionSets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
        rel="stylesheet">
     <style>th, td {
    white-space: nowrap;
}</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="form_body" runat="server">
    <div class="page-title">
        <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="title_left">
            <h3>
                Question Set</h3>
                
        </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 ">
        <ol class="breadcrumb pull-right">
        			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i> Home</a></li>
        			<li class="active"> Question Set</li>
        			<li class="active"> Question</li>
        			<li class="active">  Answer</li>
				</ol>
                </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="form-group">
             
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <asp:DropDownList ID="ddlIsactive" runat="server" CssClass="form-control" 
                            Width="111px" AutoPostBack="True" ontextchanged="ddlIsactive_TextChanged">
                         </asp:DropDownList>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <asp:Button ID="btnNew" runat="server" Text="Add Question Set" class="btn btn-primary pull-right" 
                            OnClick="btnNew_Click"></asp:Button>
                        </div>
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal" Width="100%">
                        
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                 <tr>
                                        <th>
                                        </th>
                                        <th>
                                            Question Set ID
                                        </th>
                                        <th>
                                            Question Set Name
                                        </th>
                                    
                                    </tr>
                            </thead>
                            <tbody>
                                                                    <asp:Repeater ID="RptQuestionSetlist" runat="server" OnItemCommand="RptQuestionSetlist_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 100px">
                                                    <asp:Button ID="btnQuestion" Visible='<%# Eval("Role") %>' runat="server" Text="Question" class="btn btn-success btn-xs"
                                                        CommandName="Question"></asp:Button>
                                                        <asp:Button ID="btnUpdate" Visible='<%# Eval("Role") %>' runat="server" Text="Update"
                                                        class="btn btn-warning btn-xs" CommandName="Update"></asp:Button>
                                                 
                                                    <asp:Button ID="btnDelete" Visible='<%# Eval("Role") %>' runat="server" Text="Delete"
                                                        class="btn btn-danger btn-xs" CommandName="Delete"></asp:Button>
                                                </td>
                                                <td>
                                                    <asp:Label ID="dtQuestionSetID" runat="server" Text='<%# Eval("QuestionSetID")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="dtQuestionSetText" runat="server" Text='<%# Eval("QuestionSetText")%>'></asp:Label>
                                                    
                                                </td>
                                              
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                            </tbody>
                        </table>
                        </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="PanelFormQuestionSet" runat="server" Width="100%">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Form Question Set Management
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblQuestionSetID" runat="server" Text="Question Set ID" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtQuestionSetID" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <%--    <asp:TextBox ID="txtQuestionSetID" PlaceHolder="Question Set ID" runat="server" Width="400px" ReadOnly="true"
                                    class="form-control"></asp:TextBox>--%>
                                    <input  id="txtQuestionSetID" name="txtQuestionSetID" runat="server"  type="text" class="form-control" readonly="readonly" placeholder="Question Set ID" />
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblQuestionSetName" runat="server" Text="Question Set Name" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtQuestionSetName" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtQuestionSetName" PlaceHolder="Question Set Name" runat="server"
                                    Width="400px" class="form-control"></asp:TextBox><br />
                                
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-warning" OnClick="btnUpdate_Click"
                                    ValidationGroup="valQuestionSet" />
                                <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" OnClick="btnInsert_Click"
                                    ValidationGroup="valQuestionSet" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <!-- Datatables -->
    <script type="text/javascript" src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script type="text/javascript" src="../vendors/jszip/dist/jszip.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Datatables -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').DataTable({

            });
        });
    </script>
    <!-- /Datatables -->
</asp:Content>
