﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class UploadAPK : System.Web.UI.Page
    {
        private string gBranchId, gUserId, gRoleId;
        private string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }
        private void BranchDDL()
        {
            if (getBranch() == "")
            {
                ddlBranch.DataSource = BranchFacade.LoadBranch2(getBranch());
                ddlBranch.DataTextField = "BranchName";
                ddlBranch.DataValueField = "BranchID";
                ddlBranch.DataBind();
            }
            else
            {
                ddlBranch.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                ddlBranch.DataTextField = "BranchName";
                ddlBranch.DataValueField = "BranchID";
                ddlBranch.DataBind();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BranchDDL();
            }
        }
        protected void btnUpload_Click(Object sender, EventArgs e)
        {
             String fileExtension = System.IO.Path.GetExtension(fileUpload.FileName).ToLower();
             if (fileUpload.HasFile && fileExtension == ".apk")
             {
                 string filename = "Intenary.xlsx";
                 string path = Server.MapPath("~/") + filename;
                 fileUpload.SaveAs(path);
             }
             else if(!fileUpload.HasFile)
             {
                 Response.Write("<script>alert('No File Detected');</script>");
             }
             else if (fileExtension != ".apk")
             {
                 Response.Write("<script>alert('Wrong Extension');</script>");
             }
        }
    }
}