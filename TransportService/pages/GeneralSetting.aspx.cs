﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class GeneralSetting : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<string> lBranchs = new List<string>();


                if (getBranch() == "")
                {
                    lBranchs = BranchFacade.LoadBranchByID(getBranch());
                    ddlBranchID.DataSource = lBranchs;
                    ddlBranchID.DataBind();
                }
                else
                {
                    ddlBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                    ddlBranchID.DataValueField = "BranchID";
                    ddlBranchID.DataTextField = "BranchID";
                    ddlBranchID.DataBind();
                }

                var listSettings = Core.Manager.GeneralSettingsFacade.GetCurrentSettings(ddlBranchID.SelectedValue);
                foreach (var setting in listSettings)
                {
                    if (setting.name == "IDLERADIUS")
                    {
                        txtIdleRadius.Value = setting.value;
                    }
                    else if (setting.name == "IDLETIME")
                    {
                        txtIdleTime.Value = setting.value;
                    }
                }
            }
        }

        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {

            var listSettings = Core.Manager.GeneralSettingsFacade.GetCurrentSettings(ddlBranchID.SelectedValue);
            foreach (var setting in listSettings)
            {
                if (setting.name == "IDLERADIUS")
                {
                    txtIdleRadius.Value = setting.value;
                }
                else if (setting.name == "IDLETIME")
                {
                    txtIdleTime.Value = setting.value;
                }
            }

            if (listSettings.Count == 0)
            {
                txtIdleTime.Value = "";
                txtIdleRadius.Value = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var getCurrentSetting = Core.Manager.GeneralSettingsFacade.GetCurrentSettings(ddlBranchID.SelectedValue);

            List<Core.Model.mdlSettings> listSetting = new List<Core.Model.mdlSettings>();
            var mdlSetting = new Core.Model.mdlSettings();
            mdlSetting.name = "IDLERADIUS";
            mdlSetting.value = txtIdleRadius.Value;
            mdlSetting.description = "Idle Radius is in Meter";
            mdlSetting.defaultvalue = "100";
            listSetting.Add(mdlSetting);
            mdlSetting = new Core.Model.mdlSettings();
            mdlSetting.name = "IDLETIME";
            mdlSetting.value = txtIdleTime.Value;
            mdlSetting.description = "Idle Time is in Minutes";
            mdlSetting.defaultvalue = "60";
            listSetting.Add(mdlSetting);

            foreach (var temp in listSetting)
            {
                if (getCurrentSetting.Count == 0)
                {
                    Core.Manager.GeneralSettingsFacade.InsertSettings(temp.value, temp.name, temp.description, temp.defaultvalue, ddlBranchID.SelectedValue);
                }
                else
                {
                    Core.Manager.GeneralSettingsFacade.UpdateSettings(temp.value, temp.name, ddlBranchID.SelectedValue);
                }
            }

        }
    }
}