
		   var counter = 0;
		   var directionsDisplay = [];
		   var directionsService = [];
		   var map = null;
           var infowindow = [];

		   function initialize(){
			   
                var allLocations = new Array();
				var locations = new Array();
                var branchLocation = new Array();
			
				var bounds = new google.maps.LatLngBounds();
				var dataMarkers;
	
                // var dataMarkersAll;
                var startMrk;

                // for (var all = 0; all < allmrk.length; all++) {
				    // datamarkersall = allmrk[all];
				    // alllocations.push(new google.maps.latlng(datamarkersall.lat, datamarkersall.lng));
				    // bounds.extend(alllocations[alllocations.length-1]);
				// }
                
                
//			   for(var i=0; i< branchMrk.length; i++){
//                    
//                    dataMarkers = branchMrk[i];
//                    if (dataMarkers.lng != "" && dataMarkers.lat != ""){
//                        branchLocation.push(new google.maps.LatLng(dataMarkers.lat, dataMarkers.lng));

//                        bounds.extend(branchLocation[branchLocation.length-1]);
//                    }
//               }
//                if(branchMrk.length != 0){
//                    bounds.extend(new google.maps.LatLng( branchMrk[0].lat, branchMrk[0].lng));
//                }

				for (var i = 0; i < trackingMrk.length; i++) {
				dataMarkers = trackingMrk[i];
				locations.push(new google.maps.LatLng(dataMarkers.lat, dataMarkers.lng));
				bounds.extend(locations[locations.length-1]);
				}
				
				for (var i = 0; i < visitMrk.length; i++) {
				dataMarkers = visitMrk[i];
				allLocations.push(new google.maps.LatLng(dataMarkers.lat, dataMarkers.lng));
				bounds.extend(allLocations[allLocations.length-1]);
				}

               var mapOptions;
			   
               if (visitMrk.length == 0){
               mapOptions = {
			    center: new google.maps.LatLng(branchMrk[0].lat,branchMrk[0].lng),
//                center: new google.maps.LatLng(-6.162485, 106.843707),

                zoom: 8,
				mapTypeId: google.maps.MapTypeId.ROADMAP
				};

                map = new google.maps.Map(document.getElementById('divMap'), mapOptions);
               } else{
                        
                        if(ptTracking == '')
                            {
                               mapOptions = {
                    
                                    zoom: 12,
				                    mapTypeId: google.maps.MapTypeId.ROADMAP
				                    };
                                    
                                    map = new google.maps.Map(document.getElementById('divMap'), mapOptions);
                                    map.fitBounds(bounds);
                            }
                        else
                            {
                                mapOptions = {
                                    //center: new google.maps.LatLng(branchCoordinate[0].lat,branchCoordinate[0].lng),
                                    center: new google.maps.LatLng(trackingMrk[ptTracking].lat, trackingMrk[ptTracking].lng),
                    
                                    zoom: 20,
				                    mapTypeId: google.maps.MapTypeId.ROADMAP
				                    };

                                    map = new google.maps.Map(document.getElementById('divMap'), mapOptions);
                            }

                
				google.maps.event.addDomListener(window,'resize',function() {
				google.maps.event.trigger(map,'resize');
                
				map.fitBounds(bounds);

				});

               }
				
               
				
				
             

                // var trafficLayer =  new google.maps.TrafficLayer();
                // trafficLayer.setMap(map);
				
                for(var visit = 0;visit<visitMrk.length;visit++){

                    var dataSrc = visitMrk[visit];
                    var longlat = new google.maps.LatLng(dataSrc.lat, dataSrc.lng); 
                    addVisitMrk(longlat,visit,map,dataSrc.time,dataSrc);
          
                } 

//                if (visitMrk.length == 0){
//                    for(var branch = 0; branch<branchMrk.length;branch++){
//                        var dataSrc = branchMrk[branch];
//                        var longlat = new google.maps.LatLng(dataSrc.lat, dataSrc.lng); 
//                    }
//                }

                

				var i = locations.length;
	
	
				var index = 0;
				var maxcounter = 0;


                var seperator = 0;
				
				while (i != 0) {
					seperator = 1;
					if (i < 3) {
					var tmp_locations = new Array();
					
					for (var j = index; j < locations.length; j++) {
					tmp_locations.push(locations[j]);
					maxcounter++;
					}
					
					drawRouteMap(tmp_locations,index,maxcounter,map,trackingMrk,seperator);
					i = 0;
					index = locations.length;
					}
					
					if (i >= 3 && i <= 10) {
						
					var tmp_locations = new Array();
					
					for (var j = index; j < locations.length; j++) {
					tmp_locations.push(locations[j]);
					maxcounter++
					}
					drawRouteMap(tmp_locations,index,maxcounter,map,trackingMrk,seperator);
					i = 0;
					index = locations.length;
					}
					
					if (i >= 10) {
						var tmp_locations = new Array();
						
						for (var j = index; j < index + 9; j++) {
						tmp_locations.push(locations[j]);
						maxcounter++
						}
						drawRouteMap(tmp_locations,index,maxcounter,map,trackingMrk,seperator);
						i = i - 9;
						index = index + 9;
					}
					
						
				}
				
		   
		   }
		   
		   function drawRouteMap(locations,counter,maxcounter,map,mark,dayMark) {

			var start, end;
			var waypts = [];

			for (var k = 0; k < locations.length; k++) {
				if (k >= 1 && k <= locations.length - 2) {
					waypts.push({
					location: locations[k],
					stopover: true
					});
				}
				if (k == 0) start = locations[k];

				if (k == locations.length - 1) end = locations[k];

			}
			var request = {
				origin: start,
				destination: end,
				waypoints: waypts,
				travelMode: google.maps.TravelMode.DRIVING,
                drivingOptions: {
                        departureTime: new Date(Date.now()),
                        trafficModel: "optimistic"
                    }
				};
				
			console.log(request);

			directionsService.push(new google.maps.DirectionsService());
			var instance = directionsService.length - 1;
			directionsDisplay.push(new google.maps.DirectionsRenderer({
			preserveViewport: true,
			suppressMarkers: true
			}));
			directionsDisplay[instance].setMap(map);
			directionsService[instance].route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				console.log(status);
				directionsDisplay[instance].setDirections(response);

                if(dayMark == 1)
                {
				    for(var x=counter;x<maxcounter;x++)
				    {
					var dataSrc = mark[x];
					var longlat = new google.maps.LatLng(dataSrc.lat,dataSrc.lng);
					addMarker(longlat,x,map,dataSrc.time);
				    }
                }
                 // if(dayMark==2)
                 // {
                     // for(var x=counter;x<maxcounter;x++)
				     // {
					 // var dataSrc = mark[x];
					 // var longlat = new google.maps.LatLng(dataSrc.lat,dataSrc.lng);
					 // addMarkerTuesday(longlat,x,map,dataSrc.title);
				     // }
                 // }
                  // if(dayMark==3)
                 // {
                     // for(var x=counter;x<maxcounter;x++)
				     // {
					 // var dataSrc = mark[x];
					 // var longlat = new google.maps.LatLng(dataSrc.lat,dataSrc.lng);
					 // addMarkerWednesday(longlat,x,map,dataSrc.title);
				     // }
                 // }
                  // if(dayMark==4)
                 // {
                     // for(var x=counter;x<maxcounter;x++)
				     // {
					 // var dataSrc = mark[x];
					 // var longlat = new google.maps.LatLng(dataSrc.lat,dataSrc.lng);
					 // addMarkerThursday(longlat,x,map,dataSrc.title);
				     // }
                 // }

                  // if(dayMark==5)
                 // {
                     // for(var x=counter;x<maxcounter;x++)
				     // {
					 // var dataSrc = mark[x];
					 // var longlat = new google.maps.LatLng(dataSrc.lat,dataSrc.lng);
					 // addMarkerFriday(longlat,x,map,dataSrc.title);
				     // }
                 // }
				
                 // if(dayMark==6)
                 // {
                     // for(var x=counter;x<maxcounter;x++)
				     // {
					 // var dataSrc = mark[x];
					 // var longlat = new google.maps.LatLng(dataSrc.lat,dataSrc.lng);
					 // addMarkerSaturday(longlat,x,map,dataSrc.title);
				     // }
                 // }
			}else{
				if(dayMark == 1)
                {
				    for(var x=counter;x<maxcounter;x++)
				    {
					var dataSrc = mark[x];
					var longlat = new google.maps.LatLng(dataSrc.lat,dataSrc.lng);
					addMarker(longlat,x,map,dataSrc.time);
				    }
                }
			}
			});
}
 function addVisitMrk(position,label,map,title,data){
	 var newlabel = label+1;
	 
	 var contentString = '<div id="content">' +
								'<div id="siteNotice">' +
								'</div>' +
								'<h5 id="secondHeading class="secondHeading">Visit ID : ' + data.visitID + '</h3>' +
								'<h5 id="firstHeading" class="firstHeading">Customer : ' + data.customerName + '</h1>' +
								'<h5 id="secondHeading class="secondHeading">Start Time : ' + data.startTime + '</h3>' +
                                '<h5 id="secondHeading class="secondHeading">Finish Time : ' + data.finishTime + '</h5>' +
								'<h5 id="secondHeading class="secondHeading">Reason : ' + data.reasonName + '</h5>' +
								'<h5 id="secondHeading class="secondHeading">In Radius : ' + data.isInRange + '</h5>' +
								'</div>';
     var marker = new MarkerWithLabel({
         position: position,
         map: map,
         title: title,
		 labelContent: newlabel.toString(),
         icon:'img/journey/visit.png',
         labelAnchor: new google.maps.Point(8, 30),
         labelClass: "visit", 
         labelInBackground: false,
		 zIndex:1000
     
     
     });
     var currentInfoWindow = null; 

	  var iw = new google.maps.InfoWindow({
             content: contentString
         });

     infowindow.push(iw);

     google.maps.event.addListener(marker, "click", function (e) { 
       
       for(var info= 0; info < infowindow.length; info++){
            infowindow[info].close();
       }

        iw.open(map, marker);
    

     })

 }
function addMarker(position,label,map,title){
//	var newlabel = label+1;
    var newlabel = label; //FERNANDES
	new MarkerWithLabel({
		position: position,
		map: map,
		icon:'img/journey/tracking.png',
		title:title,
        labelContent: newlabel.toString(),
        labelAnchor: new google.maps.Point(8, 40),
        labelClass: "tracking", 
        labelInBackground: false,
 });
}




 function addMarkerTuesday(position,label,map,title){
	 var newlabel = label+1;
	 new MarkerWithLabel({
		 position: position,
		 map: map,
		 title:title,
		 icon:'assets/global/images/Route/tuesday.png',
         labelContent: newlabel.toString(),
         labelAnchor: new google.maps.Point(8, 30),
         labelClass: "tuesday", 
         labelInBackground: false,
  });
 }

  function addMarkerWednesday(position,label,map,title){
	 var newlabel = label+1;
	 new MarkerWithLabel({
		 position: position,
		 map: map,
		 title:title,
		 icon:'assets/global/images/Route/wednesday.png',
         labelContent: newlabel.toString(),
         labelAnchor: new google.maps.Point(8, 30),
         labelClass: "wednesday", 
         labelInBackground: false,
  });
 }

   function addMarkerThursday(position,label,map,title){
	 var newlabel = label+1;
	 new MarkerWithLabel({
		 position: position,
		 map: map,
		 title:title,
		 icon:'assets/global/images/Route/thursday.png',
         labelContent: newlabel.toString(),
         labelAnchor: new google.maps.Point(8, 30),
         labelClass: "thursday", 
         labelInBackground: false,
  });
 }

  function addMarkerFriday(position,label,map,title){
	 var newlabel = label+1;
	 new MarkerWithLabel({
		 position: position,
		 map: map,
		 title:title,
		 icon:'assets/global/images/Route/friday.png',
         labelContent: newlabel.toString(),
         labelAnchor: new google.maps.Point(8, 30),
         labelClass: "friday", 
         labelInBackground: false,
  });
 }	
 
   function addMarkerSaturday(position,label,map,title){
	 var newlabel = label+1;
	 new MarkerWithLabel({
		 position: position,
		 map: map,
		 title:title,
		 icon:'assets/global/images/Route/saturday.png',
         labelContent: newlabel.toString(),
         labelAnchor: new google.maps.Point(8, 30),
         labelClass: "saturday", 
         labelInBackground: false,
  });
 }   
		   
		   
		  
		   
		  