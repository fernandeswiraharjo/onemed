var map, actual, iw, drag_area;
var obj, xpos, ypos;
var z_index = 0;

var geocoder;

var objOutletMarkers = [];
var objDistMarkers = [];
var arrDist = new Array();
var objJsonOutletData;
var myJSONRouting;
var myJSONDsr;
var m_strCenterMap = "";
var m_strDistroLoc = "";

var directionsDisplay; 
var directionsDisplay2;
var directionsService = new google.maps.DirectionsService();

var markerOptions = {};
var polylineOptions = {};

var bolRouteShow;

var objDsrOutlet = {
    show: function (strDsrCode) {
        var strId, strName;
        var xmlOutlet, xmlDsr;
        var strCenter = "";
        var strClass = "";
        var intCount = 1;

        for (var i = 0, m; m = objOutletMarkers[i]; i++) {
            if (m.category != "Distributor") {
                if (strDsrCode != "-1" && m.type == strDsrCode) {
                    if (m.id.substr(0, 4) == "Hid_") {
                        m.setVisible(false);
                    } else if (m.id.substr(0, 4) == "Oth_") {
                        m.setVisible(false);
                    } else {
                        m.setVisible(true);
                    }
                } else {
                    if (m.id.substr(0, 4) == "Hid_") {
                        if (strDsrCode != "-1") {
                            m.setVisible(true);
                        } else {
                            m.setVisible(false);
                        }
                    } else if (m.id.substr(0, 4) == "Oth_") {
                        m.setVisible(false);
                    } else {
                        if (strDsrCode == "-1") {
                            m.setVisible(true);
                        } else {
                            if (m.getVisible()) {
                                m.setVisible(false);
                            }
                        }
                    }
                }
            }
        }


        //Reset Outlet
        document.getElementById("na1").innerHTML = "";
        document.getElementById("na2").innerHTML = "";
        document.getElementById("na3").innerHTML = "";
        document.getElementById("na4").innerHTML = "";

        if (strDsrCode != "-1") {
            objData = objJsonOutletData;
            for (var i = 0; i < objData.length; i++) {
                objSalesList = objData[i].SalesList
                for (var y = 0; y < objSalesList.length; y++) {
                    if (strDsrCode == objSalesList[y].DsrCode) {
                        objOutletList = objSalesList[y].OutletList;

                        //Many Record per Coloumn
                        intCountPerColoumn = 0;

                        for (var x = 0; x < objOutletList.length; x++) {
                            bolExist = false;

                            if (myJSONRouting.length > 0) {
                                for (var a = 0; a < myJSONRouting.length; a++) {
                                    if (myJSONRouting[a].OutletId == objOutletList[x].OutletId) {
                                        bolExist = true;
                                        break;
                                    }
                                }
                            }
                            if (bolExist == false) {
                                intCountPerColoumn++;
                            }
                        }
                        
                        intCountPerColoumn = Math.round(intCountPerColoumn / 4, 0)
                        for (var x = 0; x < objOutletList.length; x++) {
                            bolExist = false;

                            if (myJSONRouting.length > 0) {
                                for (var a = 0; a < myJSONRouting.length; a++) {
                                    if (myJSONRouting[a].OutletId == objOutletList[x].OutletId) {
                                        bolExist = true;
                                        break;
                                    }
                                }
                            }

                            if (bolExist == false) {
                                bolFirst = true;

                                //Key Outlet
                                strKey = objOutletList[x].TypeKunjungan + "|" + objOutletList[x].OutletId;

                                switch (parseFloat(objOutletList[x].TypeKunjungan)) {
                                    case 0: strClass = "ui-state-default"; break;
                                    case 1: strClass = "ui-state-BiWeekly"; break;
                                    case 2: strClass = "ui-state-Monthly"; break;
                                }

                                v_strOutletOption = "<li class='" + strClass + "' id='" + strKey + "'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>" + objOutletList[x].OutletId + "-" + objOutletList[x].OutletNm + "</li>";

                                if (intCountPerColoumn >= intCount) {
                                    document.getElementById("na1").innerHTML += v_strOutletOption;
                                } else if ((intCountPerColoumn * 2) >= intCount) {
                                    document.getElementById("na2").innerHTML += v_strOutletOption;
                                } else if ((intCountPerColoumn * 3) >= intCount) {
                                    document.getElementById("na3").innerHTML += v_strOutletOption;
                                } else {
                                    document.getElementById("na4").innerHTML += v_strOutletOption;
                                }
                                intCount++;


                                for (var y = 0, m; m = objOutletMarkers[y]; y++) {
                                    if (m.id == strId) {
                                        bolPointExits = true;
                                        if (strCenter == "") {
                                            // Obtain the attribues of each marker
                                            lat = parseFloat(objOutletList[x].OutletLat);
                                            lng = parseFloat(objOutletList[x].OutletLng);

                                            if (!isNaN(lat) && !isNaN(lng)) {
                                                strCenter = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));

                                                map.setZoom(12);
                                                map.setCenter(strCenter);
                                                map.panTo(strCenter);
                                            }
                                        }

                                        m.setVisible(true);
                                        if (!bolFirst)
                                            break;
                                    } else if (bolFirst) {
                                        if (m.id.substr(0, 4) == "Hid_") {
                                            if (m.id.substr(4, 7) == strId) {
                                                m.setVisible(false);
                                            }
                                        }
                                    }
                                }
                            }
                            bolFirst = false;
                        }
                        break;
                    }
                }
            }
        }
    },
    hide: function () {
        var strDdlAsmCode = document.forms[0].ddlAsm.value;
        
        m_strCenterMap = new google.maps.LatLng(parseFloat(-6.1580369094572), parseFloat(106.91907707331848));
        m_strDistroLoc = new google.maps.LatLng(parseFloat(-6.1580369094572), parseFloat(106.91907707331848));

        for (var i = 0, m; m = objDistMarkers[i]; i++) {
            if (strDdlAsmCode != "") {
                if (m.id == strDdlAsmCode) {
                    // Obtain the attribues of each marker
                    var lat = m.getPosition().lat();
                    var lng = m.getPosition().lng();

                    var intLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                    m_strCenterMap = intLatLng;
                    m_strDistroLoc = intLatLng;
                }
            }
        }

        for (var i = 0, m; m = objOutletMarkers[i]; i++) {
            m.setVisible(false);
        }
        objMap.panTo(m_strCenterMap);
        objInfoWindow.close();
    }
};

function fShowOutlet(){
	with (document.forms[0]){
		$.blockUI({ message: '<h1><img src="../Image/busy.gif" /> Just a moment...</h1>' }); 
		var strAsmCode = ddlAsm.value;
		var strDsrCode = dropdownDSR.value;
		
		//Reset All Routing
		for(i = 1; i <= 4; i++){
			for(x = 1; x <= 5; x++){
				document.getElementById("w"+i+"d"+x).innerHTML = "";
			}
		}
		
		//Reset Outlet
		document.getElementById("na1").innerHTML = "";
		document.getElementById("na2").innerHTML = "";
		document.getElementById("na3").innerHTML = "";
		document.getElementById("na4").innerHTML = "";
		
		//Show Routing
		if (strDsrCode != "-1" && strDsrCode != ""){
			var objRequestOutlet;
			document.getElementById("trRouting").style.display = '';
			
			//Legend
			document.getElementById("tdDiffDay").style.display = 'none';
			document.getElementById("tdOutlet").style.display = 'none';
			document.getElementById("tdDiffOutletAll").style.display = '';

			$.ajax({
			    type: "POST",
			    url: 'WebService/GoogleWebData.asmx/GetSalesDailyRouting',
			    data: "{'strAsmCode':'" + strAsmCode + "', 'strDsrCode':'" + strDsrCode + "'}",
			    contentType: "application/json; charset=utf-8",
			    dataType: "json",
			    success: function (data, status) {
			        myJSONRouting = JSON.parse(data.d);
			        var objData = JSON.parse(data.d);

			        for (var x = 0; x < objData.length; x++) {
			            //Key Outlet
			            strKey = objData[x].TypeKunjungan + "|" + objData[x].OutletId;

			            switch (parseFloat(objData[x].TypeKunjungan)) {
			                case 0: strClass = "ui-state-default"; break;
			                case 1: strClass = "ui-state-BiWeekly"; break;
			                case 2: strClass = "ui-state-Monthly"; break;
			            }

			            v_strOutletOption = "<li class='" + strClass + "' id='" + strKey + "'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>" + objData[x].OutletId + "-" + objData[x].OutletNm + "</li>";
			            document.getElementById("w" + objData[x].VisitWeek + "d" + objData[x].VisitDay).innerHTML += v_strOutletOption;

			        }


			        //Numbering
			        fNumbering();

			        //Show Marker
			        objDsrOutlet.show(strDsrCode);

			        $.unblockUI();
			    },
			    error: function (request, status, error) {
			        alert("Error : " + request.statusText);
			    }
			});
		}else{
			document.getElementById("trRouting").style.display = 'none';
			
			//Legend
			document.getElementById("tdDiffDay").style.display = 'none';
			document.getElementById("tdOutlet").style.display = 'none';
			document.getElementById("tdDiffOutletAll").style.display = 'none';
			
			$.unblockUI();
		}
	}
}

function loadMap() { 
	with (document.forms[0]){
		$.blockUI({ message: '<h1><img src="../Image/busy.gif" /> Just a moment...</h1>' }); 
		bolRouteShow = false;

		//Create Map
		objMap = new google.maps.Map(document.getElementById("map"), objGoogleMapOption);

		//now assign to map render options
		mapRendererOptions.map = objMap;
		    
		//Load Distributor Data
		fGetDistributorMarker();
	}
}

function fLoadDistributorMarkerSuccess() {
    $.unblockUI();
}
function fLoadDistributorMarkerError(request, status, error) {
    alert("Error : " + request.statusText);
    $.unblockUI();
}

function readDataOutlet() { // Create Ajax request for XML
	with (document.forms[0]){
		if (ddlAsm.value != ""){
			$.blockUI({ message: '<h1><img src="../Image/busy.gif" /> Just a moment...</h1>' }); 
			var objRequestOutlet;
			var strAsmCode = ddlAsm.value;


			$.ajax({
			    type: "POST",
			    url: 'WebService/GoogleWebData.asmx/GetRoutingList',
			    data: "{'strAsmCode':'" + strAsmCode + "', 'strDsrCode':''}",
			    contentType: "application/json; charset=utf-8",
			    dataType: "json",
			    success: function (data, status) {
			        objJsonOutletData = JSON.parse(data.d);
			        var objData = JSON.parse(data.d);

			        $('#dropdownDSR').empty();
			        $('#dropdownDSR').append(new Option("-- Pick --", ""));

			        for (var i = 0; i < objData.length; i++) {
			            strId = objData[i].AsmCode;
			            strName = objData[i].AsmName;

			            objSalesList = objData[i].SalesList
			            for (var y = 0; y < objSalesList.length; y++) {
			                strDsrCode = objSalesList[y].DsrCode
			                strSalesName = objSalesList[y].SalesName
			                objOutletList = objSalesList[y].OutletList

			                //Insert Dsr List
			                if (cmbDepo.length == 0) {
			                    $('#dropdownDSR').append(new Option(strDsrCode + " - " + strSalesName, strDsrCode));

			                } else {
			                    if (objSalesList[y].DistDepoCd == cmbDepo.value) {
			                        $('#dropdownDSR').append(new Option(strDsrCode + " - " + strSalesName, strDsrCode));
			                    }
			                }

			                for (var x = 0; x < objOutletList.length; x++) {
			                    strId = objOutletList[x].OutletId;
			                    strName = objOutletList[x].OutletNm;
			                    strInfoWindow = "<b>" + objOutletList[x].OutletNm + "<\/b><p style='font-size:smaller'>" + objOutletList[x].OutletAddress + "<\/p>";
			                    //strInfoWindow = "";
			                    objJsonOutletData[i].SalesList[y].OutletList[x].StatusMark = "";

			                    strType = strDsrCode;

			                    //Outlet Category
			                    for (var z = 0; z < objJsonCategoryData.length; z++) {
			                        if (objJsonCategoryData[z].OutletCategory == objOutletList[x].OutletCategory) {
			                            strCategory = objJsonCategoryData[z].CategoryKey;
			                            break;
			                        }
			                    }

			                    if (objOutletList[x].OutletLat != "" && objOutletList[x].OutletLng != "") {
			                        // Obtain the attribues of each marker
			                        lat = parseFloat(objOutletList[x].OutletLat);
			                        lng = parseFloat(objOutletList[x].OutletLng);

			                        if (!isNaN(lat) && !isNaN(lng)) {
			                            objJsonOutletData[i].SalesList[y].OutletList[x].StatusMark = "Y";

			                            //Create Marker
			                            intLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));

			                            fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, strCategory, objOutletMarkers)

			                            //Hid Marker
			                            strId = "Hid_" + objOutletList[x].OutletId
			                            for (var z = 0; z < objJsonCategoryData.length; z++) {
			                                if (objJsonCategoryData[z].OutletCategory == objOutletList[x].OutletCategory) {
			                                    strCategory = objJsonCategoryData[z].CategoryKey2;
			                                    fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, strCategory, objOutletMarkers)
			                                    break;
			                                }
			                            }

			                            //Other Marker
			                            strId = "Oth_" + objOutletList[x].OutletId
			                            fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, "Other", objOutletMarkers)
			                        }
			                    }
			                }
			            }
			        }

			        // Show or hide a category initially
			        objDsrOutlet.show("-1");
			        $.unblockUI();
			    },
			    error: function (request, status, error) {
			        alert("Error : " + request.statusText);
			    }
			});
			
		}
	}
}

$(function() {
	$('#ddlAsm').val("-1");
		
	$('.pagetab').click(function() {
		show_page(extract_id($(this).attr('id')));
	});
	
	$(".sortable").sortable({
		connectWith: '.sortable',
		opacity: 0.4,
		tolerance: 'pointer',
		placeholder: 'place_holder',
		helper: function(event, el) {
			var myclone = el.clone();
			$('body').append(myclone);
			return myclone;
		},
		over: function(event, ui){
			strId = $(ui.item).attr("id");
			strTypeKunjungan = $(ui.item).attr("id").substr(0, 1);
			
			//Sender
			strWeekSender = ui.sender.attr("id").substr(0, 2);
			strDaySender = ui.sender.attr("id").substr(2, 2);
			
			//Target
			strWeekTarget = this.id.substr(0, 2);
			strDayTarget = this.id.substr(2, 2);
			
			if (strWeekSender == "na"){
				if (strTypeKunjungan == "0"){
					show_page("1");
				}else if (strTypeKunjungan == "1"){
					if (strWeekTarget == "w3" || strWeekTarget == "w4"){
						show_page("1");
					}
				}
			}
		},
		remove: function(event, ui) {

		},
		update: function(event, ui) {
			//Target
			strWeekTarget = this.id.substr(0, 2);
			strDayTarget = this.id.substr(2, 2);
			
			bolUpdate = true;
		},
		drop: function(event, ui) {

		},
		receive: function(event,ui) {
			
		},
		beforeStop: function(event,ui) {
			//Target
			strWeekSender = $(this).attr('id').substr(0, 2);
			strDaySender = $(this).attr('id').substr(2, 2);
		},
		stop: function(event, ui){
			var objList = []; 	
			var intIndex;
			var arrRepeater = [];
			
			if (bolUpdate){
				//li Atribute
				strId = $(ui.item).attr("id");
				strTypeKunjungan = $(ui.item).attr("id").substr(0, 1);
				
				//Get Index
				if (strWeekTarget != "na"){
					$('#'+strWeekTarget+strDayTarget+' li').each(function () {
						if ($(this).attr('id') == strId){
							intIndex = $(this).index();
						}
					});
				}
				
				if (strWeekSender == "na"){
					if (strWeekTarget != "na"){
						//List Repeater
						if (strTypeKunjungan == "0"){
							arrRepeater = [2, 3, 4];
						}else if (strTypeKunjungan == "1"){
							if (strWeekTarget == "w1"){
								arrRepeater = [3];
							}else if(strWeekTarget == "w2"){
								arrRepeater = [4];
							}
						}
					}
				}else{
					if (strWeekTarget == "na"){
						for(i = 1; i <= 4; i++){
							$('#w'+i+strDaySender+' li').each(function () {
								if ($(this).attr("id") == strId){
									$(this).remove();
								}
							});
						}
					}else{
						//Remove Clone
						for(i = 1; i <= 4; i++){
							if ('w'+i != strWeekSender){					
								$('#w'+i+strDaySender+' li').each(function () {
									if ($(this).attr("id") == strId){
										$(this).remove();
									}
								});
							}
						}
					
						//List Repeater
						if (strTypeKunjungan == "0"){
							for(i = 1; i <= 4; i++){
								if ('w'+i != strWeekSender){
									arrRepeater.push(i);
								}
							}
						}else if (strTypeKunjungan == "1"){
							switch(strWeekTarget){
								case "w1": arrRepeater = [3]; break;
								case "w2": arrRepeater = [4]; break;
								case "w3": arrRepeater = [1]; break;	
								case "w4": arrRepeater = [2]; break;
							}
						}
					}
				}
			
				for (var i = 0; i < arrRepeater.length; i++ ) {
					objList = []; 
					bolInsert = false;
					
					//Collect Object
					$('#w' + arrRepeater[i] + strDayTarget + ' li').each(function () {
						objList.push($(this));
					});
					
					//Remove List
					$('#w' + arrRepeater[i] + strDayTarget + ' li').each(function () {
						$(this).remove();
					});
					
					//Clone Data
					if (objList.length > 0){
						for (var x = 0; x < objList.length; x++ ) {
							if (x == intIndex){
								ui.item.clone().appendTo('#w' + arrRepeater[i] + strDayTarget);
								bolInsert = true;
							}
							objList[x].appendTo('#w' + arrRepeater[i] + strDayTarget);
						}
						if (!bolInsert){
							ui.item.clone().appendTo('#w' + arrRepeater[i] + strDayTarget);
						}
					}else{
						ui.item.clone().appendTo('#w' + arrRepeater[i] + strDayTarget);
					}
				}
				
				fNumbering();
			}
			bolUpdate = false;
		}
	}).disableSelection();
	
	$(".pagetab").droppable({
		over: function(event, ui) {
			bolvalidChange = false;
			bolElementExist = false;
			strId = ui.draggable.attr('id')
			strTypeKunjungan = ui.draggable.attr('id').substr(0, 1);
			strWeekPage = extract_id($(this).attr('id'))
			
			for(i = 1; i <= 4; i++){
				if (i != strWeekPage){
					for(x = 1; x <= 5; x++){
						$('#w' + i + 'd' + x + ' li').each(function () {
							if ($(this).attr('id') == strId){
								bolElementExist = true;
							}
						});
					}
				}
			}
			
			if (bolElementExist == false){
				if (strTypeKunjungan == "1"){
					if (strWeekPage != "3" && strWeekPage != "4"){
						bolvalidChange = true;
					}
				}
				
				if (bolvalidChange){
					$(".pagetab").removeClass('pagetab_drop');
					$(this).addClass('pagetab_drop')
					show_page(extract_id($(this).attr('id')));
				}else{
					$(".pagetab").removeClass('pagetab_drop');
					$(this).addClass('pagetab_drop')
					show_page("1");
				}
			}
		},
		out: function(event, ui) {
			$(".pagetab").removeClass('pagetab_drop');
		},
		tolerance: 'pointer'
	});
});

function extract_id(str) {
	return str.substr(str.indexOf('_')+1,str.length+1);
}

function fNumbering(){
	with (document.forms[0]){
		var intCount = 0;
		
		for(i = 1; i <= 4; i++){
			for(x = 1; x <= 5; x++){
				var lis = document.getElementById("w"+i+"d"+x).getElementsByTagName("li");
				if (intCount < lis.length)
					intCount = lis.length;
			}
		}
							
		for(x = 1; x <= 4; x++){
			document.getElementById("w" + x + "d0").innerHTML = "";
			
			for(y = 1; y <= intCount; y++){
				document.getElementById("w" + x + "d0").innerHTML += "<li class='ui-state-default'><b>" + y + "</b></li>";					
			}
		}
	}
}

function fLoad(){
	with (document.forms[0]){
		document.getElementById("trDsr").style.display = 'none';
		
		document.getElementById("trRouting").style.display = 'none';
		
		//Legend
		document.getElementById("tdDiffDay").style.display = 'none';
		document.getElementById("tdOutlet").style.display = 'none';
		document.getElementById("tdDiffOutletAll").style.display = 'none';
		
		//Load Map
		loadMap();
		
		for(i = 1; i <= 4; i++){
			for(x = 0; x <= 5; x++){
				document.getElementById("w"+i+"d"+x).innerHTML = "";
			}
		}
	}
}

function show_page(id) {
	$('.page').removeClass('page_active');
	$('#page_'+id).addClass('page_active');
}

function fRouteLine(strLi){
	with (document.forms[0]){
		var lis = document.getElementById(strLi).getElementsByTagName("li");
		var objRoute = [];

		//Block Page
		$.blockUI({ message: '<h1><img src="../Image/busy.gif" /> Just a moment...</h1>' }); 

		//Legend
		document.getElementById("tdDiffDay").style.display = '';
		document.getElementById("tdOutlet").style.display = '';
		document.getElementById("tdDiffOutletAll").style.display = 'none';
		
		//Hide all Marker
		objDsrOutlet.hide();
		
		for (var i=0; i< lis.length; i++) {
			for (var x= 0, m; m = objOutletMarkers[x]; x++) {
				if (m.type == dropdownDSR.value){
					if (m.id.substr(0,4) != "Hid_"){
						v_arrListId = lis[i].id.split("|");
						
						if (v_arrListId[1] == m.id){
							var lat = m.getPosition().lat();
							var lng = m.getPosition().lng();
							
							m.setVisible(true);
							intLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
							objRoute.push(intLatLng);
							
							break;
						}
					}
				}
			}
        }

        fGetRoutingLine(objRoute);
		
		for (var x= 0, m; m = objOutletMarkers[x]; x++) {
		    if (m.type == dropdownDSR.value) {
				if (m.id.substr(0,4) == "Hid_"){
					bolExist = false;
					for (var i=0; i< lis.length; i++) {
						v_arrListId = lis[i].id.split("|");
						if (v_arrListId[3] == m.id.substr(4,7)){
							bolExist = true;
							break;
						}
					}
					if (!bolExist)
						m.setVisible(true);
				}
			}else{
				if (m.id.substr(0,4) == "Oth_"){
					m.setVisible(true);
				}
			}
        }

        $.unblockUI();
	}
}

function fSubmit(){
    with (document.forms[0]) {
		var lis;
		var strList = "";
		var bolInsert;
		var arrValue = new Array();
		var msg = new Array();
		
		//strKey = strTypeKunjungan + "|" + strId;
		for(x = 1; x <= 4; x++){					
			for(y = 1; y <= 5; y++){
				objUl = 'w' + x + 'd' + y;
			
				$('#' + objUl + ' li').each(function () {
					intIndex = $(this).index();
					v_arrListId = $(this).attr('id').split("|");

					var message = { 'OutletId' : v_arrListId[1], 'VisitWeek' : x, 'VisitDay' : y, 'VisitOrder' : intIndex + 1 };
					arrValue.push(message);
				});
			}
		}
		
		
		$.ajax({
			type: "POST",
			url: 'WebService/GoogleWebData.asmx/SavingRouting',
			data: { Type: "Saving", DsrCode: dropdownDSR.value, Data: JSON.stringify(arrValue) },
			dataType : 'text', 
			success: function (data) {
				alert("Data has been save")
			},
			error: function (request) {
				alert("Error : " + request);
			}
		});
	}
}

function fChoseAsm(){
    with (document.forms[0]) {
        fclearOverlays(objOutletMarkers);

        if (ddlAsm.value != "") {
            $.blockUI({ message: '<h1><img src="../Image/busy.gif" /> Just a moment...</h1>' });

            bolMarker = false;
            cmbDepo.options.length = 0;

            for (var i = 0; i < objJsonDistributorData.length; i++) {
                strAsmCode = objJsonDistributorData[i].AsmCode;
                if (strAsmCode == ddlAsm.value) {
                    objList = objJsonDistributorData[i].Depo;

                    if (objList.length > 1) {
                        if (objList.length == 1) {
                            $("#spanDepo").show();
                            $("#cmbDepo").hide();

                            $("#spanDepo").html(objList[0].DistDepoCd + " - " + objList[0].DistDepoNm);

                            document.getElementById("trDepo").style.display = '';
                            document.getElementById("trDsr").style.display = '';

                            fGotoDistributorMarker(strAsmCode, objList[0].DistDepoCd);
                            readDataOutlet();
                        } else {
                            $("#spanDepo").hide();
                            $("#cmbDepo").show();

                            //Write Into Drop Down
                            oOption = document.createElement("OPTION");
                            oOption.text = "--- Pick Depo ---";
                            oOption.value = "";
                            cmbDepo.add(oOption);
                            for (var x = 0; x < objList.length; x++) {
                                oOption = document.createElement("OPTION");
                                oOption.text = objList[x].DistDepoCd + " - " + objList[x].DistDepoNm;
                                oOption.value = objList[x].DistDepoCd;
                                cmbDepo.add(oOption);
                            }

                            document.getElementById("trDepo").style.display = '';
                            document.getElementById("trDsr").style.display = 'none';

                            fGotoDistributorMarker(strAsmCode, "");
                            $.unblockUI();
                        }
                    } else {
                        document.getElementById("trDepo").style.display = 'none';
                        document.getElementById("trDsr").style.display = '';

                        fGotoDistributorMarker(strAsmCode, "");
                        readDataOutlet();
                    }

                    break;
                }
            }

			
		}else{
			//Reset Dropdown Sales
			$('#dropdownDSR').empty();
			$('#dropdownDSR').append(new Option("-- Pick --", ""));
			
            document.getElementById("trDepo").style.display = 'none';
			document.getElementById("trDsr").style.display = 'none';
			document.getElementById("trRouting").style.display = 'none';
			
			//Legend
			document.getElementById("tdDiffDay").style.display = 'none';
			document.getElementById("tdOutlet").style.display = 'none';
			document.getElementById("tdDiffOutletAll").style.display = 'none';
		}
	}
}

function fValidateDepo(){
	with (document.forms[0]){
		if (cmbDepo.value != ""){
			$.blockUI({ message: '<h1><img src="../Image/busy.gif" /> Just a moment...</h1>' }); 
			document.getElementById("trDsr").style.display = '';

			fGotoDistributorMarker(ddlAsm.value, cmbDepo.value);
			readDataOutlet();
		}else{
			document.getElementById("trDsr").style.display = 'none';
		}
	}
}