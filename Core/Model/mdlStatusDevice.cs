﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
    public class mdlStatusDevice
    {
        public string Device { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public Boolean isDownload { get; set; }
        public Boolean isFinish { get; set; }
    }
}
