﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
    public class mdlAccount
    {
        public string AccountID { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }

        public string Link { get; set; }
    }
}
