﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
    public class mdlOOS
    {
        public string Account { get; set; }
        public string BranchID { get; set; }
        public int VisitWeek { get; set; }
        public int Listed { get; set; }
        public int OutOfStock { get; set; }
        public decimal OOS { get; set; }
   }

    public class mdlOOSByBranchSKU
    {
        
        public string BranchID { get; set; }
        public int VisitWeek { get; set; }
        public int Listed { get; set; }
        public int OutOfStock { get; set; }
        public string ProductID { get; set; }
        public string Link { get; set; }
        public decimal OOS { get; set; }
    }

    public class mdlOOSByAccountBranchSKU
    {
        public string BranchID { get; set; }
        public string Account { get; set; }
        public int VisitWeek { get; set; }
        public int Listed { get; set; }
        public int OutOfStock { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Link { get; set; }
        public decimal OOS { get; set; }
    }



    public class mdlOOSByAccountSKU
    {
      
        public string Account { get; set; }
        public int VisitWeek { get; set; }
        public int Listed { get; set; }
        public int OutOfStock { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Link { get; set; }
        public decimal OOS { get; set; }
    }

    public class mdlOOSArea
    {
        public string Area { get; set; }
        public string Link { get; set; }
    }

    public class mdlOOSChart
    {
        public string ID { get; set; }
        public string json { get; set; }
        public string Area { get; set; }
    }

    public class mdlOOSChartData
    {
        public string week { get; set; }
        public decimal OOS { get; set; }
    }

    public class mdlOOSProduct
    {
        public string Account { get; set; }
        public int VisitWeek { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public int OOSCustomer { get; set; }
       

    }

    public class mdlOOSProductPerBranch
    {
        public string BranchID { get; set; }
        public int VisitWeek { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public int OOSCustomer { get; set; }
        public string Link { get; set; }


    }

    public class mdlOOSProductPerAccountBranch
    {
        public string BranchID { get; set; }
        public string Account { get; set; }
        public int VisitWeek { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public int OOSCustomer { get; set; }
        public string Link { get; set; }


    }

    public class mdlOOSProductFinal
    {
        public string Account { get; set; }
        public int VisitWeek { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public int OOSCustomer { get; set; }
        public string Link { get; set; }
    }

    public class mdlOOSCustomer
    {
        public string Account { get; set; }
        public int VisitWeek { get; set; }
        public string ProductID { get; set; }
        public string CustomerID { get; set; }
        public string Status { get; set; }
        public string Channel { get; set; }
        public string BranchID { get; set; }
        public string CustomerName { get; set; }

    }

   
}
