﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Core.Model;

namespace Core.Manager
{
    public class QuestionFacade :Base.Manager
    {

        public static Model.mdlQuestion LoadQuestionbyAnswerID(string lAnswerID)
        {
            Globals.gKey = "LoadQuestionbyAnswerID";
            var mdlQuestion = new Model.mdlQuestion();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lAnswerID}
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestionbyAnswerIDzz", sp);
            foreach (DataRow dr in dt.Rows)
            {
                
                mdlQuestion.AnswerID = dr["AnswerID"].ToString();
                mdlQuestion.AnswerTypeID = dr["AnswerTypeID"].ToString();
                mdlQuestion.IsActive = Boolean.Parse(dr["IsActive"].ToString());
                mdlQuestion.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
                mdlQuestion.Mandatory = Boolean.Parse(dr["Mandatory"].ToString());
                mdlQuestion.No = dr["No"].ToString();
                mdlQuestion.QuestionCategoryID = dr["QuestionCategoryID"].ToString();
                mdlQuestion.QuestionID = dr["QuestionID"].ToString();
                mdlQuestion.QuestionSetID = dr["QuestionSetID"].ToString();
                mdlQuestion.QuestionText = dr["QuestionText"].ToString();
                mdlQuestion.Sequence = Int32.Parse(dr["Sequence"].ToString());


            }

            return mdlQuestion;
        }

        public static List<Model.mdlQuestion> LoadQuestion(String lQuestionSetID, Boolean IsSubQuestion, Boolean IsActive)
        {
            Globals.gKey = "LoadQuestion";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestion", sp);
            var mdlQuestionList = new List<Model.mdlQuestion>();
            foreach (DataRow dr in dt.Rows)
            {
                var mdlQuestion = new Model.mdlQuestion();
                mdlQuestion.AnswerID = dr["AnswerID"].ToString();
                mdlQuestion.AnswerTypeID = dr["AnswerTypeID"].ToString();
                mdlQuestion.IsActive = Boolean.Parse(dr["IsActive"].ToString());
                mdlQuestion.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
                mdlQuestion.Mandatory = Boolean.Parse(dr["Mandatory"].ToString());
                mdlQuestion.No = dr["No"].ToString();
                mdlQuestion.QuestionCategoryID = dr["QuestionCategoryID"].ToString();
                mdlQuestion.QuestionID = dr["QuestionID"].ToString();
                mdlQuestion.QuestionSetID = dr["QuestionSetID"].ToString();
                mdlQuestion.QuestionText = dr["QuestionText"].ToString();
                mdlQuestion.Sequence = Int32.Parse(dr["Sequence"].ToString());

                mdlQuestion.IsMultiple = Boolean.Parse( dr["IsMultiple"].ToString());
                mdlQuestion.QuestionCategoryText = dr["QuestionCategoryText"].ToString();
                mdlQuestion.AnswerTypeText = dr["AnswerTypeText"].ToString();

                mdlQuestionList.Add(mdlQuestion);
            }

            return mdlQuestionList;
        }

        public static Int32 LoadQuestionSeq(string lQuestionSetID)
        {
            Globals.gKey = "LoadQuestionSeq";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID}
            };

            Int32 lSeq = 0;
            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestionSeq", sp);
            var mdlAnswerList = new List<Model.mdlAnswer>();
            foreach (DataRow dr in dt.Rows)
            {
                lSeq = Int32.Parse(dr["Sequence"].ToString()) + 1;
            }

            return lSeq;
        }

        public static String InsertQuestion(Model.mdlQuestion lParam)
        {
            Globals.gKey = "InsertQuestion";
            Globals.gReturn_Status = "";

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerTypeID},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParam.IsActive},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParam.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionText},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = lParam.User}

            };
            Globals.gReturn_Status = Manager.DataFacade.GetSP_Void(@"spInsertQuestion", sp);
            return Globals.gReturn_Status;
        }

        public static String InsertQuestionNotMultiple(Model.mdlQuestion lParamQuestion, Model.mdlAnswer lParamAnswer, Boolean lIsSubQuestion, String lQuestionID)
        {
            Globals.gReturn_Status = "";
            List<SqlParameter> sp1 = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.AnswerTypeID},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParamQuestion.IsActive},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParamQuestion.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParamQuestion.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionText},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParamQuestion.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = Globals.gUserId}

            };

            lParamAnswer.AnswerID = AnswerFacade.GenerateAnswerID(lIsSubQuestion);
            List<SqlParameter> sp2 = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParamAnswer.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerText", SqlDbType = SqlDbType.NText, Value = lParamAnswer.AnswerText},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParamAnswer.QuestionID},
                new SqlParameter() {ParameterName = "@SubQuestion", SqlDbType = SqlDbType.Bit, Value = lParamAnswer.SubQuestion},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParamAnswer.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParamAnswer.Sequence},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParamAnswer.No},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParamAnswer.IsActive},
                new SqlParameter() {ParameterName = "@CreatedBy", SqlDbType = SqlDbType.NVarChar, Value = Globals.gUserId},
            };

            if (lQuestionID == "")
            {
                Globals.gReturn_Status = Manager.DataFacade.TransSP_Void_2sp(@"spInsertQuestion", sp1, "spInsertAnswer", sp2);
            }
            else
            {
                List<SqlParameter> sp3 = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID},
                    new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.AnswerID},
                };

                Globals.gReturn_Status = Manager.DataFacade.TransSP_Void_2sp(@"spInsertQuestion", sp1, "spInsertAnswer", sp2, "spUpdateAnswerToSub", sp3);
            }

            return Globals.gReturn_Status;
        }

        public static String UpdateQuestion(Model.mdlQuestion lParam)
        {
            Globals.gKey = "UpdateQuestion";
            Globals.gReturn_Status = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerTypeID},  
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParam.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionText},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.Int, Value = lParam.User}
            };
            Globals.gReturn_Status = Manager.DataFacade.GetSP_Void("spUpdateQuestion", sp);

            return Globals.gReturn_Status;
        }

        public static String NonActiveQuestion(String lQuestionID)
        {
            Globals.gKey = "NonActiveQuestion";
            Globals.gReturn_Status = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = Globals.gUserId}
            };
            Globals.gReturn_Status = Manager.DataFacade.GetSP_Void("spNonActiveQuestion", sp);

            return Globals.gReturn_Status;
        }

        public static String InsertSubQuestion(Model.mdlQuestion lParam, string lQuestionID)
        {
            Globals.gReturn_Status = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionText},
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerTypeID},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParam.IsActive},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParam.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},            
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID}, 
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = Globals.gUserId}

            };

            List<SqlParameter> sp2 = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID},
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
            };
            Globals.gReturn_Status = Manager.DataFacade.TransSP_Void_2sp("spInsertQuestion", sp, "spUpdateAnswerToSub", sp2);
            return Globals.gReturn_Status;
        }

        public static String GenerateQuestionID(Boolean IsSubQuestion)
        {
            Globals.gKey = "GenerateQuestionID";
            String lQuestionID = "";
            int runningNo = 0;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion}
            };

            DataTable dt = Manager.DataFacade.GetSP("spGetLastQuestionID", sp);
            foreach (DataRow dr in dt.Rows)
            {
                String ID = dr["QuestionID"].ToString().Split('-')[3];
                runningNo = Int32.Parse(ID) + 1;
            }
            String code = "";
            if (IsSubQuestion == false)
                code = "QNM-";
            else
                code = "QNS-";


            lQuestionID = code + DateTime.Now.ToString("yyyy-MM-") + runningNo.ToString("0000");

            return lQuestionID;
        }

        //public static Model.mdlQuestion LoadQuestionbyId(String lQuestionSetID, Boolean IsSubQuestion, String lQuestionID)
        //{
        //    List<SqlParameter> sp = new List<SqlParameter>()
        //    {
        //        new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID},
        //        new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID},
        //        new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion}
        //    };

        //    DataTable dt = Manager.DataFacade.GetSP("spLoadQuestionbyId", sp);
        //    var mdlQuestion = new Model.mdlQuestion();
        //    foreach (DataRow dr in dt.Rows)
        //    {

        //        mdlQuestion.AnswerID = dr["AnswerID"].ToString();
        //        mdlQuestion.AnswerTypeID = dr["AnswerTypeID"].ToString();
        //        mdlQuestion.IsActive = Boolean.Parse(dr["IsActive"].ToString());
        //        mdlQuestion.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
        //        mdlQuestion.Mandatory = Boolean.Parse(dr["Mandatory"].ToString());
        //        mdlQuestion.No = dr["No"].ToString();
        //        mdlQuestion.QuestionCategoryID = dr["QuestionCategoryID"].ToString();
        //        mdlQuestion.QuestionID = dr["QuestionID"].ToString();
        //        mdlQuestion.QuestionSetID = dr["QuestionSetID"].ToString();
        //        mdlQuestion.QuestionText = dr["QuestionText"].ToString();
        //        mdlQuestion.Sequence = Int32.Parse(dr["Sequence"].ToString());


        //    }

        //    return mdlQuestion;
        //}
    }
}
