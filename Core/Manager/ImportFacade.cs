﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;

namespace Core.Manager
{
    public class ImportFacade
    {

        public static string InsertCallPlan(Model.mdlImportCallPlanFinal mdlCallPlan)
        {
            string result = DataFacade.DTSQLListInsert(mdlCallPlan.callPlan, "CallPlan");
            if(result == "1")
            {
                result = DataFacade.DTSQLListInsert(mdlCallPlan.callPlanDetail, "CallPlanDetail");
            }

            return result;
            

        }

        public static void DeleteMultipleCallPlan(Model.mdlImportCallPlanFinal mdlCallPlan)
        {
             using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString))
             {
                 try
                 {
                     conn.Open();
                 string tableName = "TempCallPlan";
                 SqlCommand cmd = new SqlCommand("CREATE TABLE ##" + tableName + " (CallPlanID varchar(50),EmployeeID varchar(50),Date date,BranchID varchar(50))", conn);
                cmd.ExecuteNonQuery();

                DataTable localTempTable = new DataTable(tableName);

                DataColumn id = new DataColumn();
                id.DataType = System.Type.GetType("System.String");
                id.ColumnName = "CallPlanID";
                localTempTable.Columns.Add(id);

                DataColumn employeeid = new DataColumn();
                employeeid.DataType = System.Type.GetType("System.String");
                employeeid.ColumnName = "EmployeeID";
                localTempTable.Columns.Add(employeeid);

                DataColumn date = new DataColumn();
                date.DataType = System.Type.GetType("System.DateTime");
                date.ColumnName = "Date";
                localTempTable.Columns.Add(date);

                DataColumn branchid = new DataColumn();
                branchid.DataType = System.Type.GetType("System.String");
                branchid.ColumnName = "BranchID";
                localTempTable.Columns.Add(branchid);

                foreach (var item in mdlCallPlan.callPlan)
                {
                    DataRow row = localTempTable.NewRow();
                    row["CallPlanID"] = item.CallPlanID;
                    row["EmployeeID"] = item.EmployeeID;
                    row["Date"] = item.Date;
                    row["BranchID"] = item.BranchID;
                    localTempTable.Rows.Add(row);
                }

                localTempTable.AcceptChanges();
                
                    
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                    {
                        bulkCopy.BulkCopyTimeout = 660;
                        bulkCopy.DestinationTableName = "##" + tableName;
                        bulkCopy.WriteToServer(localTempTable);
                        bulkCopy.Close();

                    }

                    string query = @"DELETE a FROM CallPlan a INNER JOIN ##" + tableName + " b ON a.EmployeeID = b.EmployeeID AND a.[Date] = b.[Date] AND a.BranchID = b.BranchID; DROP TABLE ##"+tableName;
                    string query1 = @"DELETE c FROM CallPlan a INNER JOIN ##TempCallPlan b ON a.EmployeeID = b.EmployeeID AND a.[Date] = b.[Date] AND a.BranchID = b.BranchID INNER JOIN CallPlanDetail c ON a.CallPlanID = c.CallPlanID;";


                    using (SqlCommand command = new SqlCommand(query1, conn))
                    {
                        command.ExecuteNonQuery();
                    }

                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {

                }
                finally
                {
                    conn.Close();
                }
                 


                




             }


           

        }

        public static void DeleteMultipleCallPlanDetail(Model.mdlImportCallPlanFinal mdlCallPlan)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString))
            {
                try
                {
                    conn.Open();
                    string tableName = "TempCallPlanDetail";
                    SqlCommand cmd = new SqlCommand("CREATE TABLE ##" + tableName + " (CallPlanID varchar(50),CustomerID varchar(50))", conn);
                    cmd.ExecuteNonQuery();

                    DataTable localTempTable = new DataTable(tableName);

                    DataColumn id = new DataColumn();
                    id.DataType = System.Type.GetType("System.String");
                    id.ColumnName = "CallPlanID";
                    localTempTable.Columns.Add(id);

                    DataColumn cid = new DataColumn();
                    cid.DataType = System.Type.GetType("System.String");
                    cid.ColumnName = "CustomerID";
                    localTempTable.Columns.Add(cid);

                    foreach (var item in mdlCallPlan.callPlanDetail)
                    {
                        DataRow row = localTempTable.NewRow();
                        row[0] = item.CallPlanID;
                        row[1] = item.CustomerID;
                        localTempTable.Rows.Add(row);
                    }

                    localTempTable.AcceptChanges();


                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                    {
                        bulkCopy.BulkCopyTimeout = 660;
                        bulkCopy.DestinationTableName = "##" + tableName;
                        bulkCopy.WriteToServer(localTempTable);
                        bulkCopy.Close();

                    }
                    
                    string query = @"DELETE a FROM CallPlanDetail a INNER JOIN ##" + tableName + " b ON a.CallPlanID = b.CallPlanID AND a.CustomerID = b.CustomerID;DROP TABLE ##" + tableName;
                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {

                }
                finally
                {
                    conn.Close();
                }








            }




        }


        public static string InsertCustomer(List<Model.Customer> customer)
        {
            return DataFacade.DTSQLListInsert(customer, "Customer");
        }

        public static string InsertEmployee(List<Model.Employee> employee)
        {
            return DataFacade.DTSQLListInsert(employee, "Employee");
        }

        public static string InsertProduct(List<Model.Product> product)
        {
            return DataFacade.DTSQLListInsert(product, "Product");
        }

        

        public static string InsertCompetitor(List<Model.Competitor> competitor)
        {
            return DataFacade.DTSQLListInsert(competitor, "Competitor");
        }
        public static string InsertCompetitorActivity(List<Model.Promo> competitorActivity)
        {
            return DataFacade.DTSQLListInsert(competitorActivity, "Promo");
        }

        public static string InsertCompetitorProduct(List<Model.CompetitorProduct> competitorProduct)
        {
            return DataFacade.DTSQLListInsert(competitorProduct, "CompetitorProduct");
        }

        public static string InsertPOSMProduct(List<Model.POSMProduct> POSMProduct)
        {
            return DataFacade.DTSQLListInsert(POSMProduct, "POSMProduct");
        }
        public static string InsertPromo(List<Model.Promo> Promo)
        {
            return DataFacade.DTSQLListInsert(Promo, "Promo");
        }


        public static void DeleteCustomer()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM Customer",sp);
        }

        public static void DeleteEmployee()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM Employee", sp);
        }
        public static void DeleteProduct()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM Product", sp);
        }

        public static void DeleteCompetitor()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM Competitor", sp);
        }

        public static void DeleteCompetitorActivity()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM Promo WHERE PromoCategory = 'COMPETITOR'", sp);
        }
        public static void DeleteCompetitorProduct()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM CompetitorProduct", sp);
        }

        public static void DeletePOSMProduct()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM POSMProduct", sp);
        }

        public static void DeletePromo()
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            string result = DataFacade.DTSQLVoidCommand("DELETE FROM Promo WHERE PromoCategory = 'SISASTOCK'", sp);
        }






        public static Model.mdlImportCallPlanFinal ReadCallPlan(string createdBy,string path)
        {


            DataTable excel = ReadExcel(path, "SELECT [EmployeeID],[BranchID],[Date],[Time],[CustomerID],[Sequence] from [Sheet1$] ");

            //DataTable excel = ReadExcel(path, "SELECT * from [Sheet1$] ");
            

            var listImportCallPlan = new List<Model.mdlImportCallPlan>();

            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.mdlImportCallPlan();
                    model.EmployeeID = row[0].ToString().Trim();
                    model.BranchID = row[1].ToString().Trim();
                    model.Date = row[2].ToString().Trim();
                    model.Time = row[3].ToString().Trim();
                    model.CustomerID = row[4].ToString().Trim();
                    model.Sequence = row[5].ToString().Trim();

                    listImportCallPlan.Add(model);
                }
            }

            //listImportCallPlan = listImportCallPlan.OrderBy(fld => fld.Date).ToList();
            listImportCallPlan = listImportCallPlan.OrderBy(fld => fld.Date).OrderBy(fld => fld.Sequence).ToList();

            var listCallPlan = new List<Model.CallPlan>();
            var listCallPlanDetail = new List<Model.CallPlanDetail>();

            int callPlanCounter = 0;



            foreach (var row in listImportCallPlan)
            {
                //if (row.Date != "" || row.EmployeeID != "")
                
                if (row.Sequence == "1")
                {
                    var mdlCallPlan = new Model.CallPlan();
                    var mdlCallPlanDetail = new Model.CallPlanDetail();

                    string employeeID = row.EmployeeID.ToString();
                    //string[] arrEmp = employeeID.Split('-');

                    callPlanCounter++;

                    mdlCallPlan.CallPlanID = "CP-" + Convert.ToDateTime(row.Date).ToString("ddMMyyyy") + "-" + employeeID ;
                    mdlCallPlan.EmployeeID = employeeID;
                    mdlCallPlan.BranchID = row.BranchID.ToString();
                    mdlCallPlan.VehicleID = "";
                    mdlCallPlan.Date = Convert.ToDateTime(row.Date);
                    mdlCallPlan.CreatedDate = DateTime.Now;
                    mdlCallPlan.LastDate = DateTime.Now;
                    mdlCallPlan.CreatedBy = createdBy;
                    mdlCallPlan.LastUpdateBy = "";
                    mdlCallPlan.IsFinish = false;
                    mdlCallPlan.Helper1 = "";
                    mdlCallPlan.Helper2 = "";
                    mdlCallPlan.FileName = "";
                    mdlCallPlan.IsDownload = false;

                    listCallPlan.Add(mdlCallPlan);


                    mdlCallPlanDetail.CallPlanID = mdlCallPlan.CallPlanID;
                    mdlCallPlanDetail.CustomerID = row.CustomerID;
                    mdlCallPlanDetail.WarehouseID = "";
                    mdlCallPlanDetail.Time = Convert.ToDateTime(row.Time.Replace(" AM","").Replace(" PM","")).TimeOfDay;
                    mdlCallPlanDetail.Sequence = Convert.ToInt32(row.Sequence);
                    mdlCallPlanDetail.IsDownload = false;
                    listCallPlanDetail.Add(mdlCallPlanDetail);
                }
                
                else
                {
                    //if (row.EmployeeID != "" && row.Date != "")
                    //{
                        var mdlCallPlanDetail = new Model.CallPlanDetail();

                        string employeeID = row.EmployeeID.ToString();
                        //string[] arrEmp = employeeID.Split('-');
                        mdlCallPlanDetail.CallPlanID = "CP-" + Convert.ToDateTime(row.Date).ToString("ddMMyyyy") + "-" + employeeID ;
                        mdlCallPlanDetail.CustomerID = row.CustomerID;
                        mdlCallPlanDetail.WarehouseID = "";
                        mdlCallPlanDetail.Time = Convert.ToDateTime(row.Time).TimeOfDay;
                        mdlCallPlanDetail.Sequence = Convert.ToInt32(row.Sequence);
                        mdlCallPlanDetail.IsDownload = false;
                        listCallPlanDetail.Add(mdlCallPlanDetail);
                    //}

                }
                




            }

            var finalCallPlan = new Model.mdlImportCallPlanFinal();
            finalCallPlan.callPlan = listCallPlan;
            finalCallPlan.callPlanDetail = listCallPlanDetail;


            return finalCallPlan;

        }


        public static List<Model.Customer> ReadCustomer(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [CustomerID],[CustomerName],[CustomerAddress],[CustomerTypeID],[BranchID],[City],[CountryRegionCode],[Account],[Channel],[Distributor],[EmployeeID] from [Sheet1$] ");

            var listCustomer = new List<Model.Customer>();

            //int counter = 1;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.Customer();

                    //model.CustomerID = "CW-INV-" + counter.ToString("000000");  // Closed Code Not Generate Id
                    model.CustomerID = row[0].ToString();
                    model.CustomerName = row[1].ToString();
                    model.CustomerAddress = row[2].ToString();
                    model.Phone = "";
                    model.Email = "";
                    model.PIC = "";
                    model.CustomerTypeID = row[3].ToString();
                    model.Latitude = "";
                    model.Longitude = "";

                    model.BranchID = row[4].ToString();
                    model.Radius = 0;

                    model.City = row[5].ToString();
                    model.CountryRegionCode = row[6].ToString();
                    model.Blocked = false;
                    model.Account = row[7].ToString();
                    model.Channel = row[8].ToString();
                    model.Distributor = row[9].ToString();
                    model.EmployeeID = row[10].ToString();
                    listCustomer.Add(model);
                    //counter++;
                }
            }

            return listCustomer;
        }

        public static List<Model.Employee> ReadEmployee(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [EmployeeID],[EmployeeName],[EmployeeTypeID],[BranchID],[EntryDate],[OutDate],[SupervisorID] from [Sheet1$] ");

            var list = new List<Model.Employee>();

            //int counter = 1;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.Employee();

                    //model.EmployeeID = "EW-INV-" + counter.ToString("00000");
                    model.EmployeeID = row[0].ToString();
                    model.EmployeeName = row[1].ToString();
                    model.EmployeeTypeID = row[2].ToString();
                    model.BranchID = row[3].ToString();
                    model.EntryDate = Convert.ToDateTime(row[4].ToString().Trim());
                    model.OutDate = Convert.ToDateTime(row[5].ToString().Trim());
                    model.SupervisorID = row[6].ToString();

                    list.Add(model);
                    //counter++;
                }
            }

            return list;
        }

        public static List<Model.Product> ReadProduct(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [ProductID],[ProductName],[ProductWeight],[UOM],[DNR_Code],[SAP_Code],[Price] from [Sheet1$] ");

            var list = new List<Model.Product>();

            int counter = 1;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.Product();

                    model.ProductID = row[0].ToString();
                    model.ProductName = row[1].ToString();
                    model.ProductType = "";
                    model.ProductGroup = "";
                    model.ProductWeight = Convert.ToDecimal(row[2]);
                    model.UOM = row[3].ToString();
                    model.DNR_Code = row[4].ToString();
                    model.SAP_Code = row[5].ToString();
                    model.Price = Convert.ToDecimal(row[6]);

                    list.Add(model);
                    counter++;
                }
            }

            return list;
        }

        public static List<Model.Competitor> ReadCompetitor(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [CompetitorName] from [Sheet1$] ");

            var list = new List<Model.Competitor>();

            int counter = 1;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.Competitor();

                    model.CompetitorID = row[0].ToString().ToUpper();
                    model.CompetitorName = row[0].ToString();


                    list.Add(model);
                    counter++;
                }
            }

            return list;
        }

        public static List<Model.Promo> ReadCompetitorActivity(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [ActivityName] from [Sheet1$] ");

            var list = new List<Model.Promo>();

            int counter = 1;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.Promo();

                    model.PromoID = "AC" + counter.ToString("000");
                    model.PromoName = row[0].ToString();
                    model.PromoCategory = "COMPETITOR";


                    list.Add(model);
                    counter++;
                }
            }

            return list;
        }

        public static List<Model.CompetitorProduct> ReadCompetitorProduct(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [CompetitorID],[CompetitorProductName] from [Sheet1$] ");

            var list = new List<Model.CompetitorProduct>();

            int counter = 1;
            DateTime dtmNow = DateTime.Now;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.CompetitorProduct();

                    model.CompetitorID = row[0].ToString().ToUpper();
                    model.CompetitorProductID = "CPROD" + counter.ToString("000");
                    model.CompetitorProductName = row[1].ToString();
                    model.CreatedBy = createdBy;
                    model.CreatedDate = dtmNow;


                    list.Add(model);
                    counter++;
                }
            }

            return list;
        }

        public static List<Model.POSMProduct> ReadPOSMProduct(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [POSMName] from [Sheet1$] ");

            var list = new List<Model.POSMProduct>();

            int counter = 1;
            DateTime dtmNow = DateTime.Now;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.POSMProduct();

                    model.POSMID = "P" + counter.ToString("000");
                    model.POSMName = row[0].ToString();
                    model.CreatedBy = createdBy;
                    model.CreatedDate = dtmNow;


                    list.Add(model);
                    counter++;
                }
            }

            return list;
        }

        public static List<Model.Promo> ReadPromo(string createdBy, string path)
        {
            DataTable excel = ReadExcel(path, "SELECT [PromoName] from [Sheet1$] ");

            var list = new List<Model.Promo>();

            int counter = 1;
            DateTime dtmNow = DateTime.Now;
            foreach (DataRow row in excel.Rows)
            {
                if (row[0].ToString().Trim() != "")
                {
                    var model = new Model.Promo();

                    model.PromoID = "PR" + counter.ToString("000");
                    model.PromoName = row[0].ToString();
                    model.PromoCategory = "SISASTOCK";



                    list.Add(model);
                    counter++;
                }
            }

            return list;
        }

        public static DataTable ReadExcel(string path,string excelSQL)
        {
            
            string conn;
            conn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 12.0'";
            OleDbConnection oleDBCon = new OleDbConnection(conn);
            oleDBCon.Open();
            DataTable dt = oleDBCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            //string SSQL = "SELECT [PromoNam] from [Sheet1$]";
            string SSQL = excelSQL;
            OleDbDataAdapter oleDA = new OleDbDataAdapter(SSQL, conn);
            DataSet ds = new DataSet();
            oleDA.Fill(ds);
            DataTable _DtTable = ds.Tables[0];
            oleDBCon.Close();

            return _DtTable;
        }
    }
}
