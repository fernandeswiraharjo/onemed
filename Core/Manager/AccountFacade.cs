﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Core.Manager
{
    public class AccountFacade
    {
        public static List<Model.Account> GetAccount(string area)
        {

            string sql = "";
            var list = new List<Model.Account>();
            
            List<SqlParameter> sp = new List<SqlParameter>()
            {
               new SqlParameter() {ParameterName = "@area", SqlDbType = SqlDbType.NVarChar, Value = area }
            };
            if (area != "")
                sql = "select distinct a.AccountID from Account a inner join Customer b on a.AccountID = b.Account WHERE b.BranchID IN (@area)";
            else
                sql = "select distinct a.AccountID from Account a inner join Customer b on a.AccountID = b.Account";

            DataTable dt = DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.Account();
                model.AccountID = row["AccountID"].ToString();
                model.AccountName = "";
                model.Description = "";

                list.Add(model);
                
            }

            return list;
            
        }
    }
}
